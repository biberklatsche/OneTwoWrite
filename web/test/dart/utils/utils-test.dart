library markdownTests;

import 'package:unittest/unittest.dart';
import '../../../src/dart/utils/utils.dart' as Utils;

void main() {
  test('stringFormat no parameter', () {
    String test = 'Hallo';
    String result = Utils.stringFormat(test);
    expect(result, equals(test));
  });

  test('stringFormat one parameter', () {
    String test = 'Hallo %1.';
    List<String> parameters = ['du'];
    String result = Utils.stringFormat(test, parameters);
    expect(result, equals('Hallo du.'));
  });

  test('stringFormat two parameters', () {
    String test = 'Hallo %1 %2.';
    List<String> parameters = ['du','da'];
    String result = Utils.stringFormat(test, parameters);
    expect(result, equals('Hallo du da.'));
  });

  test('wrapWord one word', () {
    String test = 'Hallo';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Hallo'));
  });

  test('wrapWord two words', () {
    String test = 'Hallo zusammen';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Hallo zusammen'));
  });

  test('wrapWord one long word', () {
    String test = 'Sommernachtsbrause';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Sommerna chtsbrau se'));
  });

  test('wrapWord one long width minus', () {
    String test = 'Sommernachts-brause';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Sommerna chts -brause'));
  });

  test('wrapWord one sentence', () {
    String test = 'Das veraltete deutsche Grundgesetz.';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Das veraltet e deutsche ...etz.'));
  });

  test('wrapWord one sentence', () {
    String test = 'Eine kurze Geschichte';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Eine kurze Geschich te'));
  });

  test('wrapWord one sentence', () {
    String test = 'Markdown syntax reference guide';
    String result = Utils.wrapWord(test, 8, 24);
    expect(result, equals('Markdown syntax ...guide'));
  });
}