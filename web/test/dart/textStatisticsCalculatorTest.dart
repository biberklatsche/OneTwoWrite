import '../../src/dart/utils/utils.dart' as Utils;

void main() {
  String sToCount = '#*_-';
  /*for(int i=0;i<2;i++){
    sToCount += '';
  }*/
  print(sToCount.codeUnits);
  print('countWords() start');
  Stopwatch stopwatch = new Stopwatch()
    ..start();
  print(Utils.countWords(sToCount));
  print('countWords() executed in ${stopwatch.elapsedTicks}');
}