/// Unit tests for marked.
library markdownTests;

import 'package:unittest/unittest.dart';
import '../../../../src/dart/parser/marked/src/marked.dart';

/// Most of these tests are based on observing how showdown behaves:
/// http://softwaremaniacs.org/playground/showdown-highlight/

void main() {
  group('Paragraphs', () {

    validate('consecutive lines form a single paragraph', '''
        This is the first line.
        This is the second line.
        ''', '''
        <p>This is the first line.</p>
        <p>This is the second line.</p>
        ''');

    // TODO(rnystrom): The rules here for what happens to lines following a
    // paragraph appear to be completely arbitrary in markdown. If it makes the
    // code significantly cleaner, we should consider ourselves free to change
    // these tests.

    validate('are terminated by a header', '''
        para
        # header
        ''', '''
        <p>para</p>
        <h1>header</h1>
        ''');

    validate('are terminated by a setext header', '''
        para
        header
        ==
        ''', '''
        <p>para</p>
        <h1>header</h1>
        ''');

    validate('are terminated by a hr', '''
        para
        ___
        ''', '''
        <p>para</p>
        <hr>
        ''');

    validate('consume an unordered list', '''
        para
        * list
        ''', '''
        <p>para</p>
        <ul>
          <li>list</li>
        </ul>
        ''');

    validate('consume an ordered list', '''
        para
        1. list
        ''', '''
        <p>para</p>
        <ol>
          <li>list</li>
        </ol>
        ''');
  });

  group('Setext headers', () {
    validate('h1', '''
        text
        ===
        ''', '''
        <h1>text</h1>
        ''');

    validate('h2', '''
        text
        ---
        ''', '''
        <h2>text</h2>
        ''');

    validate('h1 on first line becomes text', '''
        ===
        ''', '''
        <p>===</p>
        ''');

    validate('h2 on first line becomes text', '''
        -
        ''', '''
        <p>-</p>
        ''');

    validate('h1 turns preceding list into text', '''
        - list
        ===
        ''', '''
        <h1>- list</h1>
        ''');

    validate('h2 turns preceding list into text', '''
        - list
        ===
        ''', '''
        <h1>- list</h1>
        ''');

    validate('h1 turns preceding blockquote into text', '''
        > quote
        ===
        ''', '''
        <h1>&gt; quote</h1>
        ''');

    validate('h2 turns preceding blockquote into text', '''
        > quote
        ===
        ''', '''
        <h1>&gt; quote</h1>
        ''');
  });

  group('Headers', () {
    validate('h1', '''
        # header
        ''', '''
        <h1>header</h1>
        ''');

    validate('h2', '''
        ## header
        ''', '''
        <h2>header</h2>
        ''');

    validate('h3', '''
        ### header
        ''', '''
        <h3>header</h3>
        ''');

    validate('h4', '''
        #### header
        ''', '''
        <h4>header</h4>
        ''');

    validate('h5', '''
        ##### header
        ''', '''
        <h5>header</h5>
        ''');

    validate('h6', '''
        ###### header
        ''', '''
        <h6>header</h6>
        ''');

    validate('trailing "#" are not removed', '''
        # header ######
        ''', '''
        <h1>header ######</h1>
        ''');

  });

  group('Blockquotes', () {
    validate('single line', '''
        > blah
        ''', '''
        <blockquote>
          <p>blah</p>
        </blockquote>
        ''');

    validate('end of line', '''
        > blah
        blah
        ''', '''
        <blockquote>
          <p>blah</p>
        </blockquote>
        <p>blah</p>
        ''');

    validate('with two paragraphs', '''
        > first
        > second
        ''', '''
        <blockquote>
          <p>first</p>
          <p>second</p>
        </blockquote>
        ''');

    validate('end', '''
        > first
        second
        ''', '''
        <blockquote>
          <p>first</p>
        </blockquote>
        <p>second</p>
        ''');

    validate('nested', '''
        > one
        >> two
        >>> three
        ''', '''
        <blockquote>
          <p>one</p>
          <blockquote>
            <p>two</p>
            <blockquote>
              <p>three</p>
            </blockquote>
          </blockquote>
        </blockquote>
        ''');

    validate('nested width two paragraphs', '''
        > one
        >> two
        >> three
        >>> four
        ''', '''
        <blockquote>
          <p>one</p>
          <blockquote>
            <p>two</p>
            <p>three</p>
            <blockquote>
              <p>four</p>
            </blockquote>
          </blockquote>
        </blockquote>
        ''');

    validate('nested width go back block level', '''
        > one
        >> two
        >>> three
        > four
        ''', '''
        <blockquote>
          <p>one</p>
          <blockquote>
            <p>two</p>
            <blockquote>
              <p>three</p>
            </blockquote>
          </blockquote>
          <p>four</p>
        </blockquote>
        ''');

    validate('nested width go back block level simple', '''
        > one
        >> two
        > three
        ''', '''
        <blockquote>
          <p>one</p>
          <blockquote>
            <p>two</p>
          </blockquote>
          <p>three</p>
        </blockquote>
        ''');
  });

  group('Unordered lists', () {
    validate('asterisk, plus and hyphen', '''
        * star
        - dash
        + plus
        ''', '''
        <ul>
          <li>star</li>
          <li>dash</li>
          <li>plus</li>
        </ul>
        ''');

    validate('sublist', '''
        - list
        -- sublist
        - list
        ''', '''
        <ul>
          <li>
            list
            <ul>
              <li>
                sublist
              </li>
            </ul>
          </li>
          <li>
            list
          </li>
        </ul>
        ''');

    validate('subsublist', '''
        --- subsublist
        ''', '''
        <ul>
          <li>
            <ul>
              <li>
                <ul>
                  <li>
                    subsublist
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
        ''');

    validate('list, sublist and subsublist', '''
        - list
        -- sublist
        --- subsublist
        ''', '''
        <ul>
          <li>list
          <ul>
            <li>sublist
            <ul>
              <li>
                subsublist
              </li>
            </ul>
            </li>
          </ul>
          </li>
        </ul>
        ''');

    validate('allow a tab after the marker', '''
        *\ta
        +\tb
        -\tc
        ''', '''
        <ul>
          <li>a</li>
          <li>b</li>
          <li>c</li>
        </ul>
        ''');

    validate('start new list if blank lines separate', '''
        * one

        * two
        ''', '''
        <ul>
          <li>one</li>
        </ul>
        <ul>
          <li>two</li>
        </ul>
        ''');

    validate('start new list if blank lines separate', '''
        *   one
        *   two

        *   three
        ''', '''
        <ul>
          <li>one</li>
          <li>two</li>
        </ul>
        <ul>
          <li>
            three
          </li>
        </ul>
        ''');

    validate('do not allow other blockitems in lists', '''
        * > quote
        * # header
        ''', '''
        <ul>
          <li>&gt; quote</li>
          <li># header</li>
        </ul>
        ''');

    validate('dont allow numbered lines after first', '''
        * a
        1. b
        ''', '''
        <ul>
          <li>a</li>
        </ul>
        <ol>
          <li>b</li>
        </ol>
        ''');

  });

  group('Ordered lists', () {
    validate('start with numbers', '''
        1. one
        45.  two
        12345. three
        ''', '''
        <ol>
          <li>one</li>
          <li>two</li>
          <li>three</li>
        </ol>
        ''');

    validate('dont allow unordered lines after first', '''
        1. a
        * b
        ''', '''
        <ol>
          <li>a</li>
        </ol>
        <ul>
          <li>b</li>
        </ul>
        ''');

    validate('sublist simple', '''
        1. list
        1.1. sublist
        ''', '''
        <ol>
          <li>list
          <ol>
            <li>
              sublist
            </li>
          </ol>
          </li>
        </ol>
        ''');

    validate('sublist', '''
        1. list
        1.1. sublist
        2. list
        ''', '''
        <ol>
          <li>list
          <ol>
            <li>
              sublist
            </li>
          </ol>
          </li>
          <li>list</li>
        </ol>
        ''');

    validate('subsublist', '''
        1.1.1. subsublist
        ''', '''
        <ol>
          <li>
            <ol>
              <li>
                <ol>
                  <li>
                    subsublist
                  </li>
                </ol>
              </li>
            </ol>
          </li>
        </ol>
        ''');

    validate('list, sublist and subsublist', '''
        1. list
        1.1. sublist
        1.1.1. subsublist
        ''', '''
        <ol>
          <li>list
          <ol>
            <li>sublist
            <ol>
              <li>
                subsublist
              </li>
            </ol>
            </li>
          </ol>
          </li>
        </ol>
        ''');
  });

  group('Horizontal rules', () {
    validate('from dashes', '''
        ---
        ''', '''
        <hr>
        ''');

    validate('from asterisks', '''
        ***
        ''', '''
        <hr>
        ''');

    validate('from underscores', '''
        ___
        ''', '''
        <hr>
        ''');

    validate('can include up to two spaces', '''
        _ _  _
        ''', '''
        <hr>
        ''');
  });

  group('Strike', () {
    validate('simple', '''before -del- after''', '''<p>before <del>del</del> after</p>''');
    validate('ignore in word', '''before test-del-after''', '''<p>before test-del-after</p>''');
    validate('ignore minus', '''a - b and b - a''', '''<p>a - b and b - a</p>''');
    validate('begin of line', '''-b- a''', '''<p><del>b</del> a</p>''');
    validate('ignore', '''a -b''', '''<p>a -b</p>''');
    validate('last of line', '''-b-''', '''<p><del>b</del> </p>''');
    validate('minus and comma', '''Test-, Test- oder Test.''', '''<p>Test-, Test- oder Test.</p>''');
  });

  group('Strong', () {
    validate('using asterisks', '''
        before **strong** after
        ''', '''
        <p>before <strong>strong</strong> after</p>
        ''');

    validate('using underscores', '''
        before __strong__ after
        ''', '''
        <p>before <strong>strong</strong> after</p>
        ''');

    validate('unmatched asterisks', '''
        before ** after
        ''', '''
        <p>before ** after</p>
        ''');

    validate('unmatched underscores', '''
        before __ after
        ''', '''
        <p>before __ after</p>
        ''');

    validate('multiple spans in one text', '''
        a **one** b __two__ c
        ''', '''
        <p>a <strong>one</strong> b <strong>two</strong> c</p>
        ''');

    validate('more than one word', '''
        before **first second** after
        ''', '''
        <p>before <strong>first second</strong> after</p>
        ''');
  });

  group('Emphasis and strong', () {
    validate('single asterisks', '''
        before *em* after
        ''', '''
        <p>before <em>em</em> after</p>
        ''');

    validate('single underscores', '''
        before _em_ after
        ''', '''
        <p>before <em>em</em> after</p>
        ''');

    validate('double asterisks', '''
        before **strong** after
        ''', '''
        <p>before <strong>strong</strong> after</p>
        ''');

    validate('double underscores', '''
        before __strong__ after
        ''', '''
        <p>before <strong>strong</strong> after</p>
        ''');

    validate('unmatched asterisk', '''
        before *after
        ''', '''
        <p>before *after</p>
        ''');

    validate('unmatched underscore', '''
        before _after
        ''', '''
        <p>before _after</p>
        ''');

    validate('multiple spans in one text', '''
        a *one* b _two_ c
        ''', '''
        <p>a <em>one</em> b <em>two</em> c</p>
        ''');

    validate('more than one word', '''
        before *first second* after
        ''', '''
        <p>before <em>first second</em> after</p>
        ''');

    /*validate('not processed when surrounded by spaces', '''
        a * b * c _ d _ e
        ''', '''
        <p>a * b * c _ d _ e</p>
        ''');*/

    /*validate('strong then emphasis', '''
        **strong***em*
        ''', '''
        <p><strong>strong</strong><em>em</em></p>
        ''');*/

    /*validate('emphasis then strong', '''
        *em***strong**
        ''', '''
        <p><em>em</em><strong>strong</strong></p>
        ''');*/

    validate('emphasis inside strong', '''
        **strong *em***
        ''', '''
        <p><strong>strong <em>em</em></strong></p>
        ''');

    validate('mismatched in nested', '''
        *a _b* c_
        ''', '''
        <p><em>a _b</em> c_</p>
        ''');

    validate('cannot nest tags of same type', '''
        *a _b *c* d_ e*
        ''', '''
        <p><em>a _b </em>c<em> d_ e</em></p>
        ''');
  });

  group('Inline code', () {
    validate('simple case', '''
        before `source` after
        ''', '''
        <p>before <code>source</code> after</p>
        ''');

    validate('unmatched backtick', '''
        before ` after
        ''', '''
        <p>before ` after</p>
        ''');
    validate('multiple spans in one text', '''
        a `one` b `two` c
        ''', '''
        <p>a <code>one</code> b <code>two</code> c</p>
        ''');

    validate('more than one word', '''
        before `first second` after
        ''', '''
        <p>before <code>first second</code> after</p>
        ''');

    validate('simple double backticks', '''
        before ``source`` after
        ''', '''
        <p>before <code>source</code> after</p>
        ''');

    validate('double backticks', '''
        before ``can `contain` backticks`` after
        ''', '''
        <p>before <code>can `contain` backticks</code> after</p>
        ''');

    validate('double backticks with spaces', '''
        before `` `tick` `` after
        ''', '''
        <p>before <code>`tick` </code> after</p>
        ''');

    validate('multiline double backticks with spaces', '''
        before ``in `tick` another`` after
        ''', '''
        <p>before <code>in `tick` another</code> after</p>
        ''');

    validate('ignore markup inside code', '''
        before `*b* _c_` after
        ''', '''
        <p>before <code>*b* _c_</code> after</p>
        ''');

    validate('escape HTML characters', '''
        `<&>`
        ''', '''
        <p><code>&lt;&amp;&gt;</code></p>
        ''');

    validate('escape HTML tags', '''
        '*' `<em>`
        ''', '''
        <p>&#39;*&#39; <code>&lt;em&gt;</code></p>
        ''');
  });

  group('Code blocks', () {
    validate('single line', '''
            code
        ''', '''
        <pre><code>code</code></pre>
        ''');

    validate('include leading whitespace after indentation', '''
            zero
             one
              two
               three
        ''', '''
        <pre><code>zero
         one
          two
           three</code></pre>
        ''');

    validate('code blocks separated by newlines form one block', '''
            zero
            one

            two

            three
        ''', '''
        <pre><code>zero
         one

         two

         three</code></pre>
        ''');

    validate('code blocks separated by two newlines form multiple blocks', '''
            zero
            one


            two


            three
        ''', '''
        <pre><code>zero
                one


                two


                three

            </code></pre>
        ''');

    validate('escape HTML characters', '''
            <&>
        ''', '''
        <pre><code>&lt;&amp;&gt;</code></pre>
        ''');
  });

  group('Fenced code blocks', () {
    validate('without an optional language identifier', '''
        ```
        code
        ```
        ''', '''
        <pre><code>code
        </code></pre>
        ''');

    validate('with an optional language identifier', '''
        ```dart
        code
        ```
        ''', '''
        <pre class="dart"><code>code
        </code></pre>
        ''');

    validate('escape HTML characters', '''
        ```
        <&>
        ```
        ''', '''
        <pre><code>&lt;&amp;&gt;
        </code></pre>
        ''');

    validate('Pandoc style without language identifier', '''
        ~~~~~
        code
        ~~~~~
        ''', '''
        <pre><code>code
        </code></pre>
        ''');

    validate('Pandoc style with language identifier', '''
        ~~~~~dart
        code
        ~~~~~
        ''', '''
        <pre class="dart"><code>code
        </code></pre>
        ''');

    validate('Pandoc style with inner tildes row', '''
        ~~~~~
        ~~~
        code
        ~~~
        ~~~~~
        ''', '''
        <pre><code>~~~
        code
        ~~~
        </code></pre>
        ''');
  });

  group('Block-level HTML', () {
    /*validate('single line', '''
        <table></table>
        ''', '''
        <table></table>
        ''');

    validate('multi-line', '''
        <table>
            blah
        </table>
        ''', '''
        <table>
            blah
        </table>
        ''');

    validate('blank line ends block', '''
        <table>
            blah
        </table>

        para
        ''', '''
        <table>
            blah
        </table>
        <p>para</p>
        ''');

    validate('HTML can be bogus', '''
        <bogus>
        blah
        </weird>

        para
        ''', '''
        <bogus>
        blah
        </weird>
        <p>para</p>
        ''');*/
  });

  group('HTML encoding', () {
    /*/*validate('less than and ampersand are escaped', '''
        < &
        ''', '''
        <p>&lt; &amp;</p>
        ''');*/
    validate('greater than is not escaped', '''
        not you >
        ''', '''
        <p>not you ></p>
        ''');
    validate('existing entities are untouched', '''
        &amp;
        ''', '''
        <p>&amp;</p>
        ''');*/
  });

  group('Autolinks', () {
    /*validate('basic link', '''
        before <http://foo.com/> after
        ''', '''
        <p>before <a href="http://foo.com/">http://foo.com/</a> after</p>
        ''');
    validate('handles ampersand in url', '''
        <http://foo.com/?a=1&b=2>
        ''', '''
        <p><a href="http://foo.com/?a=1&b=2">http://foo.com/?a=1&amp;b=2</a></p>
        ''');*/
  });

  group('Reference links', () {
    /*validate('double quotes for title', '''
        links [are] [a] awesome

        [a]: http://foo.com "woo"
        ''', '''
        <p>links <a href="http://foo.com" title="woo">are</a> awesome</p>
        ''');
    validate('single quoted title', """
        links [are] [a] awesome

        [a]: http://foo.com 'woo'
        """, '''
        <p>links <a href="http://foo.com" title="woo">are</a> awesome</p>
        ''');
    validate('parentheses for title', '''
        links [are] [a] awesome

        [a]: http://foo.com (woo)
        ''', '''
        <p>links <a href="http://foo.com" title="woo">are</a> awesome</p>
        ''');
    validate('no title', '''
        links [are] [a] awesome

        [a]: http://foo.com
        ''', '''
        <p>links <a href="http://foo.com">are</a> awesome</p>
        ''');
    validate('unknown link becomes plaintext', '''
        [not] [known]
        ''', '''
        <p>[not] [known]</p>
        ''');
    validate('can style link contents', '''
        links [*are*] [a] awesome

        [a]: http://foo.com
        ''', '''
        <p>links <a href="http://foo.com"><em>are</em></a> awesome</p>
        ''');
    validate('inline styles after a bad link are processed', '''
        [bad] `code`
        ''', '''
        <p>[bad] <code>code</code></p>
        ''');
    validate('empty reference uses text from link', '''
        links [are][] awesome

        [are]: http://foo.com
        ''', '''
        <p>links <a href="http://foo.com">are</a> awesome</p>
        ''');
    validate('references are case-insensitive', '''
        links [ARE][] awesome

        [are]: http://foo.com
        ''', '''
        <p>links <a href="http://foo.com">ARE</a> awesome</p>
        ''');*/
  });

  group('Inline links', () {
    /*validate('double quotes for title', '''
        links [are](http://foo.com "woo") awesome
        ''', '''
        <p>links <a href="http://foo.com" title="woo">are</a> awesome</p>
        ''');
    validate('no title', '''
        links [are] (http://foo.com) awesome
        ''', '''
        <p>links <a href="http://foo.com">are</a> awesome</p>
        ''');
    validate('can style link contents', '''
        links [*are*](http://foo.com) awesome
        ''', '''
        <p>links <a href="http://foo.com"><em>are</em></a> awesome</p>
        ''');*/
  });

  group('Inline Images', () {
    /*validate('image', '''
        ![](http://foo.com/foo.png)
        ''', '''
        <p>
          <a href="http://foo.com/foo.png">
            <img src="http://foo.com/foo.png"></img>
          </a>
        </p>
        ''');

    validate('alternate text', '''
        ![alternate text](http://foo.com/foo.png)
        ''', '''
        <p>
          <a href="http://foo.com/foo.png">
            <img alt="alternate text" src="http://foo.com/foo.png"></img>
          </a>
        </p>
        ''');

    validate('title', '''
        ![](http://foo.com/foo.png "optional title")
        ''', '''
        <p>
          <a href="http://foo.com/foo.png" title="optional title">
            <img src="http://foo.com/foo.png" title="optional title"></img>
          </a>
        </p>
        ''');
    validate('invalid alt text', '''
        ![`alt`](http://foo.com/foo.png)
        ''', '''
        <p>
          <a href="http://foo.com/foo.png">
            <img src="http://foo.com/foo.png"></img>
          </a>
        </p>
        ''');*/
  });

  group('Reference Images', () {
    /*validate('image', '''
        ![][foo]
        [foo]: http://foo.com/foo.png
        ''', '''
        <p>
          <a href="http://foo.com/foo.png">
            <img src="http://foo.com/foo.png"></img>
          </a>
        </p>
        ''');

    validate('alternate text', '''
        ![alternate text][foo]
        [foo]: http://foo.com/foo.png
        ''', '''
        <p>
          <a href="http://foo.com/foo.png">
            <img alt="alternate text" src="http://foo.com/foo.png"></img>
          </a>
        </p>
        ''');

    validate('title', '''
        ![][foo]
        [foo]: http://foo.com/foo.png "optional title"
        ''', '''
        <p>
          <a href="http://foo.com/foo.png" title="optional title">
            <img src="http://foo.com/foo.png" title="optional title"></img>
          </a>
        </p>
        ''');

    validate('invalid alt text', '''
        ![`alt`][foo]
        [foo]: http://foo.com/foo.png "optional title"
        ''', '''
        <p>
          <a href="http://foo.com/foo.png" title="optional title">
            <img src="http://foo.com/foo.png" title="optional title"></img>
          </a>
        </p>
        ''');*/

  });
}

/**
 * Removes eight spaces of leading indentation from a multiline string.
 *
 * Note that this is very sensitive to how the literals are styled. They should
 * be:
 *     '''
 *     Text starts on own line. Lines up with subsequent lines.
 *     Lines are indented exactly 8 characters from the left margin.'''
 *
 * This does nothing if text is only a single line.
 */
// TODO(nweiz): Make this auto-detect the indentation level from the first
// non-whitespace line.

String cleanUpLiteral(String text) {
  var lines = text.split('\n');
  if (lines.length <= 1) return text;

  for (var j = 0; j < lines.length; j++) {
    if (lines[j].length > 8) {
      lines[j] = lines[j].substring(8, lines[j].length);
    } else {
      lines[j] = '';
    }
  }

  return lines.join('\n');
}

void validate(String description, String markdown, String html, {bool verbose: false, inlineSyntaxes, linkResolver}) {
  test(description, () {
    markdown = cleanUpLiteral(markdown);
    html = cleanUpLiteral(html);

    var result = markdownToHtml(markdown);
    var passed = compareOutput(html, result);

    if (!passed) {
      // Remove trailing newline.
      html = html.substring(0, html.length - 1);

      var sb = new StringBuffer();
      sb.writeln('Expected: ${html.replaceAll("\n", "\n          ")}');
      sb.writeln('  Actual: ${result.replaceAll("\n", "\n          ")}');

      fail(sb.toString());
    }
  });
}

/// Does a loose comparison of the two strings of HTML. Ignores differences in
/// newlines and indentation.

bool compareOutput(String a, String b) {
  int i = 0;
  int j = 0;

  skipIgnored(String s, int i) {
    // Ignore newlines.
    while ((i < s.length) && (s[i] == '\n')) {
      i++;
      // Ignore indentation.
      while ((i < s.length) && (s[i] == ' ')) i++;
    }

    return i;
  }

  while (true) {
    i = skipIgnored(a, i);
    j = skipIgnored(b, j);

    // If one string runs out of non-ignored strings, the other must too.
    if (i == a.length) return j == b.length;
    if (j == b.length) return i == a.length;

    if (a[i] != b[j]) return false;
    i++;
    j++;
  }
}
