# Markdown syntax reference guide
- - - 
**OneTwoWrite** supports a subset of the Markdown formatting language. This lets you apply basic formatting by adding a few punctuation characters, such as \*emphasis\* and \*\*importance\*\*. 
- - -
## Headings
````
    # Title = First level heading
    ## Title = Second level heading
    ### Title = Third level heading
````
…up to six levels

- - - 
## Emphasis and Importance
**Emphasis:** `*example*` or `_example_` 
**Importance:** `**example**` or `__example__` 

- - -
## Lists

**Numbered lists:** type `1. ` then a space
```
    1. Ordered list item
    2. Ordered list item
    3. Ordered list item
```
Any number (followed by a full stop and space) can be used — list items will be ordered from 1 when exported.

**Bulleted lists:** type `*`, `-` or `+` then a space 
Create a bulleted list by using an asterisk (`*`), hyphen (`-`), or plus sign (`+`), followed by a space. For example:
```
    * Bulleted list item
    * Bulleted list item
    * Bulleted list item
```
**Nesting lists:**
You can also nest lists several levels deep, and combine them:
```
    * First level
    ** Second level

    1. First level
    1.1. Second level
```

- - - 
## Blockquotes
Type `> ` plus a space (just like email):
```
    > A quoted paragraph
    >> A quoted paragraph inside a quotation
```

- - - 
## Links
Create a link by surrounding the link text in square brackets, followed immediately by the URL in parentheses:
```
    [text to link](http://example.com/)
```

- - -
## Code
You can mark up code in-line using backticks (`code`), or add a code block by adding three backtricks at the beginning and the end of a code block.

```
    Normal text `this is code` normal text.
```

Note that inline formatting characters (like _underscores_) are ignored in code.

- - - 
## Separating paragraphs
A line starting with a tab indicates a block of code. Because of this it is currently not possible to use a return-plus-tab to indicate a new paragraph in **OneTwoWrite**. Instead, please use one or two returns to separate paragraphs.

- - - 
## Horizontal rules
You can add a thematic break which will be represented by a dividing line (<hr>) when exported to HTML. To do so, add three or more asterisks (`*`), hyphens (`-`), or underscores (`_`) on a line by themselves, separated with spaces. For example:
```
    * * *
```
or
```
    - - -
```

- - - 
## “Escaping” formatting characters
If you want to type a formatting character and have **OneTwoWrite** treat it as text not formatting, type a backslash first \. This means `\*gives*`, `\_gives_` etc. Escaping isn’t needed in code blocks.