library bus;

import 'dart:async';
import 'dart:html';
import '../model/model.dart';

//Events main -> ...
final EventType mouseMoveEvent = new EventType();
final EventType windowResizeEvent = new EventType();
final EventType<Node> windowClickEvent = new EventType<Node>();
final EventType<Theme> themeChangedEvent = new EventType<Theme>();

//Events Buttom Menu -> Editor
final EventType<HeaderButtonClickEventData> headerButtonClickEvent = new EventType<HeaderButtonClickEventData>();
final EventType<bool> orderedListButtonClickEvent = new EventType<bool>();
final EventType<bool> unorderedListButtonClickEvent = new EventType<bool>();
final EventType<bool> blockquoteButtonClickEvent = new EventType<bool>();
final EventType<bool> italicButtonClickEvent = new EventType<bool>();
final EventType<bool> boldButtonClickEvent = new EventType<bool>();
final EventType<bool> strikeButtonClickEvent = new EventType<bool>();

//Events Left Menu -> Editor
final EventType<String> loadDocumentEvent = new EventType<String>();
final EventType newDocumentEvent = new EventType();
final EventType saveDocumentEvent = new EventType();
final EventType copyDocumentEvent = new EventType();
final EventType deleteDocumentEvent = new EventType();
final EventType<String> searchInDocumentEvent = new EventType<String>();
final EventType<String> searchNextInDocumentEvent = new EventType<String>();
final EventType<String> searchPreviousInDocumentEvent = new EventType<String>();
final EventType<String> replaceInDocumentEvent = new EventType<String>();
final EventType<String> replaceAllInDocumentEvent = new EventType<String>();
final EventType<String> downloadDocumentEvent = new EventType<String>();
final EventType<String> importDocumentEvent = new EventType<String>();
final EventType<FontSize> fontSizeChangeEvent = new EventType<FontSize>();

//Events Editor -> Bottom Menu
final EventType<int> headerLineHasFocusEvent = new EventType<int>();
final EventType orderedListLineHasFocusEvent = new EventType();
final EventType unorderedListLineHasFocusEvent = new EventType();
final EventType blockquoteLineHasFocusEvent = new EventType();
final EventType normalLineHasFocusEvent = new EventType();
final EventType strikeRangeHasFocusEvent = new EventType();
final EventType boldRangeHasFocusEvent = new EventType();
final EventType italicRangeHasFocusEvent = new EventType();
final EventType normalRangeHasFocusEvent = new EventType();
final EventType textStatisticsChangedEvent = new EventType<Map>();
final EventType userIsTypingEvent = new EventType<bool>();

//Event Model -> Top Menu
final EventType<bool> documentEditedEvent = new EventType<bool>();
final EventType<String> documentNameChangedEvent = new EventType<String>();
final EventType<bool> focusModeChangedEvent = new EventType<bool>();

//Event Left Menu -> Preview
final EventType<bool> showHtmlPreviewEvent = new EventType<bool>();

//Event Editor -> Preview
final EventType<String> htmlPreviewCalculatedEvent = new EventType<String>();

class EventBus {
  bool sync;

  EventBus({this.sync: true});

  /// Map containing a stream controller for each [EventType]
  Map<EventType, StreamController> streamControllers = new Map<EventType, StreamController>();

  Stream/*<T>*/
  on(EventType/*<T>*/
     eventType) {
    return streamControllers.putIfAbsent(eventType, () {
      return new StreamController.broadcast(sync: sync);
    }).stream;
  }

  void fire(EventType/*<T>*/
            eventType, /*<T>*/
            [data]) {
    if (data != null && !eventType.isTypeT(data)) {
      throw new ArgumentError('Provided data is not of same type as T of EventType.');
    }

    var controller = streamControllers[eventType];
    if (controller != null) {
      controller.add(data);
    }
  }
}

class EventType<T> {

  String name;

  EventType([this.name]);

  bool isTypeT(data) => data is T;
}

class HeaderButtonClickEventData {
  bool active;
  int headerType;
}