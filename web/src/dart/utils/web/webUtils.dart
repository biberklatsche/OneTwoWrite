library utils;
import '../../parser/marked/src/marked.dart' as Mark;
import 'dart:js';

String calcInlineHtml(String markdown) {
  return Mark.markdownToHtml(markdown);
}

String calcHtml(String title, String markdown) {
  String markdownContent = calcInlineHtml(markdown);
  String html = '<!doctype html><html><head><title>$title</title><meta charset="UTF-8"></head><body>$markdownContent</body></html>';
  return html;
}

String calcPdf(String title, String markdown) {
  String html = calcInlineHtml(markdown);
  JsObject jsPDF = new JsObject(context['jsPDF']);
  var mapOptions = new JsObject.jsify({
      "width": 170
  });
  jsPDF.callMethod('fromHTML', [html]);
  var out = jsPDF.callMethod('output');
  return out;
}