part of menu.left;

class LeftMenuViewPresenter implements LeftMenuViewListener {
  final EventBus _bus;
  final LeftMenuView _view;
  final Model _model;
  bool isEditedWarningShown = false;
  static final int DOCUMENT_ENTRY_WIDTH_IN_PX = 80;

  LeftMenuViewPresenter(this._view, this._bus, this._model) {
    _view.setListener(this);
    _bus.on(userIsTypingEvent).listen((isTyping) {
      if (isTyping) {
        _view.hideMenu();
      } else {
        _view.showMenu();
      }
    });

    //Event vom Main, wenn in das Fenster geklickt wurde.
    _bus.on(windowClickEvent).listen((Node node) {
      Element parent = node.parent;
      while (parent != null && parent.id != 'menu-left') {
        parent = parent.parent;
      }
      if (parent == null) {
        _view.closeMenu();
      }
    });

    //Event vom Main, wenn in das Fenster geklickt wurde.
    _bus.on(documentEditedEvent).listen((bool edited) {
      if (edited != isEditedWarningShown) {
        isEditedWarningShown = edited;
        _view.showEditedWarning(edited);
      }
    });
  }

  void onReplaceCheckboxClick(bool active) {
    _view.showReplaceField(active);
  }

  void onDocumentsButtonClick(bool isActive) {
    _view.showDocumentEntries(!isActive);
    if (!isActive) {
      _view.clearDocumentEntries();
      _calcDocumentsContainerWidthInPx().then((int widthInPx) {
        _view.setDocumentsContainerWidthInPx(widthInPx);
        _model.getAllDocumentNames().then((List<String> documentNames) {
          for (String documentName in documentNames) {
            _view.addDocumentEntry(documentName, Utils.wrapWord(documentName, 8, 24));
          }
        });
      });
    }
  }

  void onDocumentEntryClick(String documentName) {
    String currentDocumentName = _view.getCurrentDetailDocumentName();
    if (currentDocumentName.isNotEmpty) {
      _view.reduceIconOfDocumentEntry(currentDocumentName);
      if (documentName == currentDocumentName) {
        _view.removeDocumentDetails();
      } else {
        int newDocumentEntryIndex = _view.getIndexOfDocumentEntry(documentName);
        int newRow = _calcRow(newDocumentEntryIndex);
        int newColumn = _calcColumn(newDocumentEntryIndex);
        int currentDocumentEntryIndex = _view.getIndexOfDocumentEntry(currentDocumentName);
        int currentRow = _calcRow(currentDocumentEntryIndex);
        int arrowPositionInPX = (DOCUMENT_ENTRY_WIDTH_IN_PX / 2).floor() + DOCUMENT_ENTRY_WIDTH_IN_PX * newColumn;
        if (newRow == currentRow) {
          _model.getDocument(documentName).then((EditorDocument doc) {
            _view.enlargeIconOfDocumentEntry(documentName);
            _view.updateDocumentDetails(doc, arrowPositionInPX);
          });
        } else {
          _view.removeDocumentDetails();
          int index = _calcIndexForEntryContainer(newRow);
          _model.getDocument(documentName).then((EditorDocument doc) {
            _view.enlargeIconOfDocumentEntry(documentName);
            _view.addDocumentDetails(doc, index, arrowPositionInPX, _model.getSettings().theme);
          });
        }
      }
    } else {
      int newDocumentEntryIndex = _view.getIndexOfDocumentEntry(documentName);
      int newColumn = _calcColumn(newDocumentEntryIndex);
      int newRow = _calcRow(newDocumentEntryIndex);
      int arrowPositionInPX = (DOCUMENT_ENTRY_WIDTH_IN_PX / 2).floor() + DOCUMENT_ENTRY_WIDTH_IN_PX * newColumn;
      int index = _calcIndexForEntryContainer(newRow);
      _model.getDocument(documentName).then((EditorDocument doc) {
        _view.enlargeIconOfDocumentEntry(documentName);
        _view.addDocumentDetails(doc, index, arrowPositionInPX, _model.getSettings().theme);
      });
    }
  }

  int _calcRow(int index) {
    int documentsPerRow = (_view.getDocumentsContainerWidth() / DOCUMENT_ENTRY_WIDTH_IN_PX).floor();
    int row = (index / documentsPerRow).floor();
    return row;
  }

  int _calcColumn(int index) {
    int documentsPerRow = (_view.getDocumentsContainerWidth() / DOCUMENT_ENTRY_WIDTH_IN_PX).floor();
    int column = index % documentsPerRow;
    return column;
  }

  int _calcIndexForEntryContainer(int row) {
    int documentsPerRow = (_view.getDocumentsContainerWidth() / DOCUMENT_ENTRY_WIDTH_IN_PX).floor();
    int index = (row + 1) * documentsPerRow;
    return index;
  }

  void onDocumentEntryDoubleClick(String documentName) {
    _view.closeMenu();
    _model.isNewDocument = false;
    _model.isEdited = false;
    _model.currentDocumentName = documentName;
    _bus.fire(loadDocumentEvent, documentName);
  }

  void onDocumentMenuButtonClick() {
    if (_model.isNewDocument && _model.isEdited) {
      _view.showSaveButton(false);
      _view.showSaveAsButton(true);
      _view.enableDeleteButton(false);
      _view.enableCopyButton(false);
    } else if (_model.isNewDocument && !_model.isEdited) {
      _view.showSaveButton(false);
      _view.showSaveAsButton(true);
      _view.enableDeleteButton(false);
      _view.enableCopyButton(false);
    } else if (!_model.isNewDocument && _model.isEdited) {
      _view.showSaveButton(true);
      _view.showSaveAsButton(false);
      _view.enableDeleteButton(true);
      _view.enableCopyButton(true);
    } else if (!_model.isNewDocument && !_model.isEdited) {
      _view.showSaveButton(true);
      _view.showSaveAsButton(false);
      _view.enableDeleteButton(true);
      _view.enableCopyButton(true);
    }
  }

  void onNewButtonClick() {
    _view.closeMenu();
    _bus.fire(newDocumentEvent);
  }

  void onSaveButtonClick() {
    _bus.fire(saveDocumentEvent);
    _view.closeMenu();
  }

  void onCopyButtonClick() {
    _bus.fire(copyDocumentEvent);
    _view.closeMenu();
  }

  void onDeleteButtonClick() {
    _bus.fire(deleteDocumentEvent);
    _view.closeMenu();
  }

  void onDetailsDeleteButtonClick(String documentName) {
    Dialog.openConfirmDialog(I18n.translate(I18n.DIALOG_DELETE_DOCUMENT, [documentName]), (bool confirm) {
      if (confirm) {
        _model.deleteDocument(documentName).then((_) {
          _view.removeIconOfDocumentEntry(documentName);
          _view.removeDocumentDetails();
          if (documentName == _model.currentDocumentName) {
            _bus.fire(newDocumentEvent);
          }
        });

      }
    });
  }

  void onCloseDocumentDetailsButtonClick() {
    String currentDocumentName = _view.getCurrentDetailDocumentName();
    _view.reduceIconOfDocumentEntry(currentDocumentName);
    _view.removeDocumentDetails();
  }

  Future<int> _calcDocumentsContainerWidthInPx() {
    return _model.countDocuments().then((int value) {
      int containerWidthInPx = (value / 2 ).floor() * DOCUMENT_ENTRY_WIDTH_IN_PX;
      return containerWidthInPx < 530 ? 530 : containerWidthInPx;
    });
  }

  void onSearchButtonClick(String searchText) {
    _bus.fire(searchInDocumentEvent, searchText);
  }

  void onSearchNextButtonClick(String searchText) {
    _bus.fire(searchNextInDocumentEvent, searchText);
  }

  void onSearchPreviousButtonClick(String searchText) {
    _bus.fire(searchPreviousInDocumentEvent, searchText);
  }

  void onReplaceButtonClick(String searchText, String replaceText) {
    if (!searchText.isEmpty) {
      _bus.fire(replaceInDocumentEvent, replaceText);
    }
  }

  void onReplaceAllButtonClick(String searchText, String replaceText) {
    if (!searchText.isEmpty) {
      _bus.fire(replaceAllInDocumentEvent, replaceText);
    }
  }

  void onExportMdButtonClick() {
    _bus.fire(downloadDocumentEvent, DownloadDocumentType.MD.toString());
    _view.closeMenu();
  }

  void onExportPdfButtonClick() {
    _bus.fire(downloadDocumentEvent, DownloadDocumentType.PDF.toString());
    _view.closeMenu();
  }

  void onExportHtmlButtonClick() {
    _bus.fire(downloadDocumentEvent, DownloadDocumentType.HTML.toString());
    _view.closeMenu();
  }

  void onImportButtonClick() {
    Dialog.openFileUploadDialog((String content) {
      if (content != null) {
        _view.closeMenu();
        _model.isNewDocument = true;
        _model.isEdited = true;
        _model.currentDocumentName = I18n.translate(I18n.DEFAULT_DOCUMENT_NAME);
        _bus.fire(importDocumentEvent, content);
      }
    });
  }

  void onSettingsMenuButtonClick() {
    Settings settings = _model.getSettings();
    switch (settings.fontSize) {
      case FontSize.SMALL:
        _view.setSettingsFontSizeSmall();
        break;
      case FontSize.MEDIUM:
        _view.setSettingsFontSizeMedium();
        break;
      case FontSize.LARGE:
        _view.setSettingsFontSizeLarge();
        break;
      default:
        _view.setSettingsFontSizeAuto();
        break;
    }
    switch (settings.theme) {
      case Theme.Dark:
        _view.setSettingsThemeDark();
        break;
      default:
        _view.setSettingsThemeLight();
        break;
    }
    _view.setSettingAutoSave(settings.autoSafe);
    _view.setSettingShowPreview(settings.showPreview);
  }

  void onFontSizeButtonClick(FontSize fontSize) {
    Settings settings = _model.getSettings();
    settings.fontSize = fontSize;
    _model.saveSettings(settings);
    _bus.fire(fontSizeChangeEvent, fontSize);
  }

  void onAutoSaveButtonClick(bool autoSave) {
    Settings settings = _model.getSettings();
    settings.autoSafe = autoSave;
    _model.saveSettings(settings);
  }

  void onShowPreviewButtonClick(bool showPreview) {
    Settings settings = _model.getSettings();
    settings.showPreview = showPreview;
    _model.saveSettings(settings);
    _bus.fire(showHtmlPreviewEvent, showPreview);
  }

  void onThemeButtonClick(Theme theme) {
    Settings settings = _model.getSettings();
    settings.theme = theme;
    _model.saveSettings(settings);
    _bus.fire(themeChangedEvent, theme);
  }
}

class LeftMenuViewImpl extends LeftMenuView {
  Element menu;
  LeftMenuViewListener listener;
  Element documentsContainer;
  var deleteButtonListener;
  var copyButtonListener;
  var searchValueChangedTimer;

  LeftMenuViewImpl() {
    _init();
  }

  void showReplaceField(bool show) {
    if (show) {
      querySelector('#replace-container').classes.remove('display-none');
    } else {
      querySelector('#replace-container').classes.add('display-none');
    }

  }

  void _init() {
    menu = querySelector('#menu-left');

    //Dateien
    documentsContainer = querySelector('#documents-container');
    querySelector('#button-menu-documents').onClick.listen((Event event) {
      _handleDocumentMenuButtonClick(event.target);
    });
    querySelector('#button-new').onClick.listen((Event event) {
      _handleNewButtonClick(event.target);
    });
    querySelector('#button-save').onClick.listen((Event event) {
      _handleSaveButtonClick(event.target);
    });
    querySelector('#button-save-as').onClick.listen((Event event) {
      _handleSaveButtonClick(event.target);
    });
    copyButtonListener = querySelector('#button-copy').onClick.listen((Event event) {
      _handleCopyButtonClick(event.target);
    });
    deleteButtonListener = querySelector('#button-delete').onClick.listen((Event event) {
      _handleDeleteButtonClick(event.target);
    });
    querySelector('#button-menu-documents-container').onClick.listen((Event event) {
      _handleDocumentsButtonClick(event.target);
    });
    querySelector('#button-export-md').onClick.listen((Event event) {
      _handleExportMdButtonClick(event.target);
    });
    querySelector('#button-export-pdf').onClick.listen((Event event) {
      _handleExportPdfButtonClick(event.target);
    });
    querySelector('#button-export-html').onClick.listen((Event event) {
      _handleExportHtmlButtonClick(event.target);
    });
    querySelector('#button-import').onClick.listen((Event event) {
      _handleImportButtonClick(event.target);
    });

    //Suche
    querySelector('#button-menu-search').onClick.listen((Event event) {
      _handleSearchButtonClick(event.target);
    });
    querySelector('#button-search-previous').onClick.listen((Event event) {
      _handleSearchPreviousButtonClick(event.target);
    });
    querySelector('#button-search-next').onClick.listen((Event event) {
      _handleSearchNextButtonClick(event.target);
    });
    querySelector('#input-search').onKeyUp.listen((Event event) {
      _handleSearchFieldValueChanged(event.target);
    });
    querySelector('#checkbox-replace').onChange.listen((Event event) {
      _handleCheckboxReplaceValueChanged(event.target);
    });
    querySelector('#button-replace').onClick.listen((Event event) {
      _handleReplaceButtonClick(event.target);
    });
    querySelector('#button-replace-all').onClick.listen((Event event) {
      _handleReplaceAllButtonClick(event.target);
    });

    //Settings
    querySelector('#button-menu-settings').onClick.listen((Event event) {
      _handleSettingsMenuButtonClick(event.target);
    });
    querySelector('#font-size-button-auto').onClick.listen((Event event) {
      _handleFontSizeButtonClick(event.target);
    });
    querySelector('#font-size-button-small').onClick.listen((Event event) {
      _handleFontSizeButtonClick(event.target);
    });
    querySelector('#font-size-button-medium').onClick.listen((Event event) {
      _handleFontSizeButtonClick(event.target);
    });
    querySelector('#font-size-button-large').onClick.listen((Event event) {
      _handleFontSizeButtonClick(event.target);
    });
    querySelector('#preview-button-on').onClick.listen((Event event) {
      _handleShowPreviewButtonClick(event.target);
    });
    querySelector('#preview-button-off').onClick.listen((Event event) {
      _handleShowPreviewButtonClick(event.target);
    });
    querySelector('#auto-save-button-on').onClick.listen((Event event) {
      _handleAutoSafeButtonClick(event.target);
    });
    querySelector('#auto-save-button-off').onClick.listen((Event event) {
      _handleAutoSafeButtonClick(event.target);
    });
    querySelector('#light-theme-button').onClick.listen((Event event) {
      _handleThemeButtonClick(event.target);
    });
    querySelector('#dark-theme-button').onClick.listen((Event event) {
      _handleThemeButtonClick(event.target);
    });

    //Help
    querySelector('#button-menu-help').onClick.listen((Event event) {
      _handleHelpMenuButtonClick(event.target);
    });
  }

  void setListener(LeftMenuViewListener listener) {
    this.listener = listener;
  }

  void setDocumentsContainerWidthInPx(int width) {
    documentsContainer.style.width = width.toString() + 'px';
  }

  void _handleDocumentMenuButtonClick(Element documentMenuButton) {
    bool active = _toggleButtonAndHideSubMenus(documentMenuButton);
    if (active) {
      listener.onDocumentMenuButtonClick();
      querySelector('#menu-documents').classes.remove('display-none');
    }
  }

  void _handleNewButtonClick(Element newButton) {
    listener.onNewButtonClick();
  }

  void _handleSaveButtonClick(Element saveButton) {
    listener.onSaveButtonClick();
  }

  void _handleCopyButtonClick(Element copyButton) {
    listener.onCopyButtonClick();
  }

  void _handleDeleteButtonClick(Element deleteButton) {
    if (deleteButton.id == 'button-delete') {
      listener.onDeleteButtonClick();
    } else {
      listener.onDetailsDeleteButtonClick(querySelector('#document-details-name').text);
    }
  }

  void _handleOpenButtonClick(Element openButton) {
    listener.onDocumentEntryDoubleClick(querySelector('#document-details-name').text);
  }

  void _handleCloseButtonClick(Element closeButton) {
    listener.onCloseDocumentDetailsButtonClick();
  }

  void _handleExportMdButtonClick(Element exportMdButton) {
    listener.onExportMdButtonClick();
  }

  void _handleExportPdfButtonClick(Element exportDocxButton) {
    listener.onExportPdfButtonClick();
  }

  void _handleExportHtmlButtonClick(Element exportHtmlButton) {
    listener.onExportHtmlButtonClick();
  }

  void _handleImportButtonClick(Element uploadButton) {
    listener.onImportButtonClick();
  }

  void _handleDocumentsButtonClick(Element documentsButton) {
    listener.onDocumentsButtonClick(documentsButton.classes.contains('active'));
  }

  void _handleSearchNextButtonClick(Element searchNextButton) {
    InputElement elem = querySelector('#input-search');
    listener.onSearchNextButtonClick(elem.value);
  }

  void _handleSearchPreviousButtonClick(Element searchNextButton) {
    InputElement elem = querySelector('#input-search');
    listener.onSearchPreviousButtonClick(elem.value);
  }

  void _handleCheckboxReplaceValueChanged(CheckboxInputElement replaceCheckbox) {
    listener.onReplaceCheckboxClick(replaceCheckbox.checked);
  }

  void _handleReplaceButtonClick(Element replaceButton) {
    InputElement search = querySelector('#input-search');
    InputElement replace = querySelector('#input-replace');
    listener.onReplaceButtonClick(search.value, replace.value);
  }

  void _handleReplaceAllButtonClick(Element replaceAllButton) {
    InputElement search = querySelector('#input-search');
    InputElement replace = querySelector('#input-replace');
    listener.onReplaceAllButtonClick(search.value, replace.value);
  }

  int getDocumentsContainerWidth() {
    return documentsContainer.clientWidth;
  }

  int getIndexOfDocumentEntry(String documentName) {
    Element document = documentsContainer.children.firstWhere((Element document) => document.id == documentName);
    var index = 0;
    while ((document = document.previousElementSibling) != null) {
      if (document.id != 'document-details') {
        index++;
      }
    }
    return index;
  }

  void enlargeIconOfDocumentEntry(String documentName) {
    Element document = documentsContainer.children.firstWhere((Element document) => document.id == documentName);
    document.children.first.style.fontSize = '370%';
  }

  void reduceIconOfDocumentEntry(String documentName) {
    Element document = documentsContainer.children.firstWhere((Element document) => document.id == documentName);
    document.children.first.style.fontSize = '290%';
  }

  void removeIconOfDocumentEntry(String documentName) {
    Element document = documentsContainer.children.firstWhere((Element document) => document.id == documentName);
    document.remove();
  }

  void clearDocumentEntries() {
    documentsContainer.children.clear();
  }

  void addDocumentEntry(final String documentId, String documentName) {
    Element icon = new DivElement();
    icon.classes.add('icon-doc-text');
    icon.classes.add('icon');
    Element document = new DivElement();
    document.id = documentId;
    document.classes.add('document');
    document.append(icon);
    document.appendText(documentName);
    document.onClick.listen((Event event) {
      listener.onDocumentEntryClick(documentId);
    });
    document.onDoubleClick.listen((Event event) {
      listener.onDocumentEntryDoubleClick(documentId);
    });
    documentsContainer.append(document);
  }

  void addDocumentDetails(EditorDocument doc, int position, int arrowPositionLeftInPX, Theme theme) {
    Element arrowTop = new SpanElement();
    arrowTop.id = 'document-details-arrow';
    arrowTop.classes.add('arrow-top');
    arrowTop.style.left = arrowPositionLeftInPX.toString() + 'px';
    Element documentName = new DivElement();
    documentName.text = doc.name;
    documentName.id = 'document-details-name';

    Element open = new ButtonElement();
    open.text = 'Open';
    open.classes.add('default');
    open.onClick.listen((Event event) {
      _handleOpenButtonClick(event.target);
    });
    Element delete = new ButtonElement();
    delete.text = 'Delete...';
    delete.onClick.listen((Event event) {
      _handleDeleteButtonClick(event.target);
    });
    Element close = new ButtonElement();
    close.id = 'button-close-document-details';
    close.classes.add('button-close');
    close.text = '×';
    close.onClick.listen((Event event) {
      _handleCloseButtonClick(event.target);
    });
    Element buttons = new DivElement();
    buttons.children.add(open);
    buttons.children.add(delete);
    buttons.children.add(close);

    Element header = new DivElement();
    header.id = 'document-details-header';
    header.children.add(documentName);

    Element preview = new PreElement();
    preview.id = 'preview';
    Element previewContainer = new DivElement();
    previewContainer.id = 'preview-container';
    previewContainer.children.add(preview);

    Element charsValue = new SpanElement();
    charsValue.id = 'document-details-chars-value';
    charsValue.text = doc.chars.toString();
    Element charsPhrase = new SpanElement();
    charsPhrase.id = 'document-details-chars-phrase';
    charsPhrase.classes.add('border-right');
    charsPhrase.text = ' Characters';
    charsPhrase.style.paddingRight = '5px';
    Element wordsPhrase = new SpanElement();
    wordsPhrase.text = ' Words';
    wordsPhrase.id = 'document-details-words-phrase';
    wordsPhrase.classes.add('border-right');
    wordsPhrase.style.paddingRight = '5px';
    Element wordsValue = new SpanElement();
    wordsValue.id = 'document-details-words-value';
    wordsValue.text = doc.words.toString();
    wordsValue.style.paddingLeft = '5px';
    Element readingTimePhrase = new SpanElement();
    readingTimePhrase.id = 'document-details-reading-time-phrase';
    readingTimePhrase.text = 'Reading Time ';
    readingTimePhrase.style.paddingLeft = '5px';
    Element readingTimeValue = new SpanElement();
    readingTimeValue.id = 'document-details-reading-time-value';
    int hours = (doc.readingTimeInSeconds / 3600).floor();
    int minutes = (doc.readingTimeInSeconds / 60).floor() % 60;
    int seconds = doc.readingTimeInSeconds % 60;
    String hoursAsString = hours < 10 ? '0$hours' : '$hours';
    String minutesAsString = minutes < 10 ? '0$minutes' : '$minutes';
    String secondsAsString = seconds < 10 ? '0$seconds' : '$seconds';
    readingTimeValue.text = '$hoursAsString:$minutesAsString:$secondsAsString';
    Element textStatistics = new DivElement();
    textStatistics.id = 'document-details-text-statistics';
    textStatistics.children.add(charsValue);
    textStatistics.children.add(charsPhrase);
    textStatistics.children.add(wordsValue);
    textStatistics.children.add(wordsPhrase);
    textStatistics.children.add(readingTimePhrase);
    textStatistics.children.add(readingTimeValue);


    Element documentDetails = new DivElement();
    documentDetails.children.add(arrowTop);
    documentDetails.children.add(buttons);
    documentDetails.children.add(new HRElement());
    documentDetails.children.add(header);
    documentDetails.children.add(previewContainer);
    documentDetails.children.add(new HRElement());
    documentDetails.children.add(textStatistics);
    documentDetails.id = 'document-details';
    if (position >= documentsContainer.children.length) {
      documentsContainer.children.add(documentDetails);
    } else {
      documentsContainer.children.insert(position, documentDetails);
    }

    JsObject previewEditor = new JsObject(context['ace']['edit'], ['preview']);
    previewEditor.callMethod('setTheme', [theme.editorTheme]);
    previewEditor.callMethod('getSession').callMethod('setMode', ['ace/mode/markdown']);
    previewEditor.callMethod('setHighlightActiveLine', [false]);
    previewEditor.callMethod('getSession').callMethod('setUseWrapMode', [true]);
    previewEditor.callMethod('setShowPrintMargin', [false]);
    previewEditor.callMethod('getSession').callMethod('setWrapLimitRange', [64, 64]);
    previewEditor['renderer'].callMethod('setShowGutter', [false]);
    previewEditor.callMethod('setValue', [doc.preview, 1]);
    previewEditor.callMethod('setReadOnly', [true, 1]);
  }

  void updateDocumentDetails(EditorDocument doc, int arrowPositionLeftInPX) {
    Element arrowTop = querySelector('#document-details-arrow');
    arrowTop.style.left = arrowPositionLeftInPX.toString() + 'px';
    Element documentName = querySelector('#document-details-name');
    documentName.text = doc.name;
    Element chars = querySelector('#document-details-chars-value');
    chars.text = doc.chars.toString();
    Element words = querySelector('#document-details-words-value');
    words.text = doc.words.toString();
    Element readingTime = querySelector('#document-details-reading-time-value');
    int hours = (doc.readingTimeInSeconds / 3600).floor();
    int minutes = (doc.readingTimeInSeconds / 60).floor() % 60;
    int seconds = doc.readingTimeInSeconds % 60;
    String hoursAsString = hours < 10 ? '0$hours' : '$hours';
    String minutesAsString = minutes < 10 ? '0$minutes' : '$minutes';
    String secondsAsString = seconds < 10 ? '0$seconds' : '$seconds';
    readingTime.text = '$hoursAsString:$minutesAsString:$secondsAsString';
    JsObject previewEditor = new JsObject(context['ace']['edit'], ['preview']);
    previewEditor.callMethod('setValue', [doc.preview, 1]);
  }

  void removeDocumentDetails() {
    Element documentDetails = querySelector('#document-details');
    if (documentDetails != null) {
      documentDetails.remove();
    }
  }


  String getCurrentDetailDocumentName() {
    Element detailsDocumentName = querySelector('#document-details-name');
    if (detailsDocumentName != null) {
      return detailsDocumentName.text;
    }
    return '';
  }

  void showDocumentEntries(bool show) {
    Element menuDocuments = querySelector('#menu-documents-container');
    Element documentsButton = querySelector('#button-menu-documents-container');
    if (show) {
      menuDocuments.classes.remove('display-none');
      documentsButton.classes.add('active');

    } else {
      menuDocuments.classes.add('display-none');
      documentsButton.classes.remove('active');
    }
  }

  void _handleSearchButtonClick(Element searchButton) {
    bool active = _toggleButtonAndHideSubMenus(searchButton);
    if (active) {
      InputElement search = querySelector('#input-search');
      InputElement replace = querySelector('#input-replace');
      querySelector('#menu-search').classes.remove('display-none');
      search.value = '';
      replace.value = '';
      if (!querySelector('#replace-container').classes.contains('display-none')) {
        querySelector('#replace-container').classes.add('display-none');
      }
      CheckboxInputElement replaceCheckBox = querySelector('#checkbox-replace');
      replaceCheckBox.checked = false;
      querySelector('#input-search').focus();
    }
  }

  _handleSearchFieldValueChanged(InputElement searchField) {
    if (searchValueChangedTimer != null && searchValueChangedTimer.isActive) {
      searchValueChangedTimer.cancel();
    }
    searchValueChangedTimer = new Timer(const Duration(milliseconds: 500), () {
      listener.onSearchButtonClick(searchField.value);
    });
  }

  void _handleSettingsMenuButtonClick(Element settingsButton) {
    bool active = _toggleButtonAndHideSubMenus(settingsButton);
    if (active) {
      querySelector('#menu-settings').classes.remove('display-none');
    }
    listener.onSettingsMenuButtonClick();
  }

  void _handleFontSizeButtonClick(Element button) {
    if (button.id == 'font-size-button-large') {
      listener.onFontSizeButtonClick(FontSize.LARGE);
    } else if (button.id == 'font-size-button-medium') {
      listener.onFontSizeButtonClick(FontSize.MEDIUM);
    } else if (button.id == 'font-size-button-small') {
      listener.onFontSizeButtonClick(FontSize.SMALL);
    } else {
      listener.onFontSizeButtonClick(FontSize.AUTO);
    }
  }

  void _handleThemeButtonClick(Element button) {
    if (button.id == 'dark-theme-button') {
      listener.onThemeButtonClick(Theme.Dark);
    } else {
      listener.onThemeButtonClick(Theme.Light);
    }
  }

  void _handleAutoSafeButtonClick(Element button) {
    listener.onAutoSaveButtonClick(button.id == 'auto-save-button-on');
  }

  void _handleShowPreviewButtonClick(Element button) {
    listener.onShowPreviewButtonClick(button.id == 'preview-button-on');
  }

  void _handleHelpMenuButtonClick(Element helpButton) {
    bool active = _toggleButtonAndHideSubMenus(helpButton);
    if (active) {
      Element menuHelp = querySelector('#menu-help');
      menuHelp.classes.remove('display-none');
      menuHelp.style.height = (window.innerHeight - 100).toString() + 'px';
      IFrameElement menuHelpIFrame = querySelector('#menu-help iframe');
      menuHelpIFrame.setAttribute('src', I18n.getHelpHtmlFilePath());
    }
  }

  bool _toggleButtonAndHideSubMenus(Element button) {
    querySelectorAll('.submenu').forEach((Element button) => button.classes.add('display-none'));
    ElementList buttons = querySelectorAll('#menu-left .button');
    if (!button.classes.contains('active')) {
      buttons.forEach((Element button) => button.classes.remove('active'));
      button.classes.add('active');
      return true;
    } else {
      buttons.forEach((Element button) => button.classes.remove('active'));
      return false;
    }
  }

  void closeMenu() {
    querySelectorAll('#menu-left .button').forEach((Element button) => button.classes.remove('active'));
    querySelectorAll('#menu-left .submenu').forEach((Element button) => button.classes.add('display-none'));
  }

  void showMenu() {
    if (menu.classes.contains('hidden')) {
      menu.classes.remove('hidden');
      menu.classes.add('visible');
    }
  }

  void hideMenu() {
    if (menu.classes.contains('visible')) {
      menu.classes.remove('visible');
      menu.classes.add('hidden');
    }
  }

  void enableCopyButton(bool enable) {
    if (enable) {
      querySelector('#button-copy').classes.remove('disable');
      copyButtonListener = querySelector('#button-copy').onClick.listen((Event event) {
        _handleCopyButtonClick(event.target);
      });
    } else {
      querySelector('#button-copy').classes.add('disable');
      copyButtonListener.cancel();
    }
  }

  void showSaveButton(bool show) {
    Element saveButton = querySelector('#button-save');
    if (show) {
      saveButton.classes.remove('display-none');
    } else {
      saveButton.classes.add('display-none');
    }
  }

  void showSaveAsButton(bool show) {
    Element saveButton = querySelector('#button-save-as');
    if (show) {
      saveButton.classes.remove('display-none');
    } else {
      saveButton.classes.add('display-none');
    }
  }

  void enableDeleteButton(bool enable) {
    if (enable) {
      querySelector('#button-delete').classes.remove('disable');
      deleteButtonListener = querySelector('#button-delete').onClick.listen((Event event) {
        _handleDeleteButtonClick(event.target);
      });
    } else {
      querySelector('#button-delete').classes.add('disable');
      deleteButtonListener.cancel();
    }
  }

  void showEditedWarning(bool show) {
    if (show) {
      querySelector('#edited-warning').classes.remove('display-none');
    } else {
      querySelector('#edited-warning').classes.add('display-none');
    }
  }

  void setSettingsFontSizeSmall() {
    RadioButtonInputElement radioButton = querySelector('#font-size-button-small');
    radioButton.checked = true;
  }

  void setSettingsFontSizeMedium() {
    RadioButtonInputElement radioButton = querySelector('#font-size-button-medium');
    radioButton.checked = true;
  }

  void setSettingsFontSizeLarge() {
    RadioButtonInputElement radioButton = querySelector('#font-size-button-large');
    radioButton.checked = true;
  }

  void setSettingsFontSizeAuto() {
    RadioButtonInputElement radioButton = querySelector('#font-size-button-auto');
    radioButton.checked = true;
  }

  void setSettingAutoSave(bool enabled) {
    RadioButtonInputElement radioButtonOn = querySelector('#auto-save-button-on');
    radioButtonOn.checked = enabled;
    RadioButtonInputElement radioButtonOff = querySelector('#auto-save-button-off');
    radioButtonOff.checked = !enabled;
  }

  void setSettingShowPreview(bool enabled) {
    RadioButtonInputElement radioButtonOn = querySelector('#preview-button-on');
    radioButtonOn.checked = enabled;
    RadioButtonInputElement radioButtonOff = querySelector('#preview-button-off');
    radioButtonOff.checked = !enabled;
  }

  void setSettingsThemeLight() {
    RadioButtonInputElement radioButton = querySelector('#light-theme-button');
    radioButton.checked = true;
  }

  void setSettingsThemeDark() {
    RadioButtonInputElement radioButton = querySelector('#dark-theme-button');
    radioButton.checked = true;
  }


}

