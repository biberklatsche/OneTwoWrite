part of menu.top;

class TopMenuViewPresenter implements TopMenuViewListener {
  final EventBus _bus;
  final TopMenuView _view;
  bool _isInFullscreenMode = false;

  TopMenuViewPresenter(this._view, this._bus) {
    _view.setListener(this);
    _bus.on(documentNameChangedEvent).listen((String name) {
      _view.setDocumentName(name);
    });

    _bus.on(userIsTypingEvent).listen((isTyping) {
      if (isTyping) {
        _view.hideMenu();
      } else {
        _view.showMenu();
      }
    });
  }

  void onFullscreenButtonClick() {
    _isInFullscreenMode = !_isInFullscreenMode;
    _view.activateFullscreenMode(_isInFullscreenMode);
    _bus.fire(windowResizeEvent);
  }
}

class TopMenuViewImpl extends TopMenuView {

  Element fullscreenButton;
  Element menuTop;
  TopMenuViewListener listener;

  TopMenuViewImpl() {
    _init();
  }

  void _init() {
    menuTop = querySelector('#menu-top');
    fullscreenButton = querySelector("#button-resize");
    fullscreenButton.onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    document.onFullscreenChange.listen((Event event) {
      _handleFullscreenChange();
    });
  }

  void setListener(TopMenuViewListener listener) {
    this.listener = listener;
  }

  void _handleButtonClick(Element button) {
    listener.onFullscreenButtonClick();
  }

  void _handleFullscreenChange() {
    if (fullscreenButton.classes.contains('icon-resize-small')) {
      fullscreenButton.classes.add('icon-resize-full');
      fullscreenButton.classes.remove('icon-resize-small');
    } else {
      fullscreenButton.classes.remove('icon-resize-full');
      fullscreenButton.classes.add('icon-resize-small');
    }
  }

  void activateFullscreenMode(bool activate) {
    JsObject resizer = new JsObject(context['jshelper']);
    if (activate) {
      resizer.callMethod('requestFullscreen');
    } else {
      resizer.callMethod('exitFullscreen');
    }
  }

  void setDocumentName(String documentName) {
    querySelector('#documentname').text = documentName;
  }

  void showMenu() {
    if (menuTop.classes.contains('hidden')) {
      menuTop.classes.remove('hidden');
      menuTop.classes.add('visible');
    }
  }

  void hideMenu() {
    if (menuTop.classes.contains('visible')) {
      menuTop.classes.remove('visible');
      menuTop.classes.add('hidden');
    }
  }
}

