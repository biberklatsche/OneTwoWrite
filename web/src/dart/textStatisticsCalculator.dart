import 'dart:isolate';
import 'utils/utils.dart' as Utils;

void main(List<String> args, SendPort replyTo) {
  String text = args.first;
  int charCount = Utils.countChars(text);
  int wordCount = Utils.countWords(text);
  int readingTimeInSeconds = Utils.calculateReadingTime(wordCount);
  var textStatistics = new Map();
  textStatistics['characters'] = charCount;
  textStatistics['words'] = wordCount;
  textStatistics['readingTimeInSeconds'] = readingTimeInSeconds;
  replyTo.send(textStatistics);
}

