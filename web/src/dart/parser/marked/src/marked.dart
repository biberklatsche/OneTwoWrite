library marked;

/**
 * Block-Level Grammar
 */

Map<String, RegExp> block = {
    'newline': new RegExp(r'^\n+'), 'code': new RegExp(r'^( {4}[^\n]+\n*)+'), 'fences': new RegExp(r'^ *(`{3,}|~{3,}) *(\S+)? *\n([\s\S]+?)\s*\1 *(?:\n+|$)'), 'hr': new RegExp(r'^( *[-*_]){3,} *(?:\n+|$)'), 'heading': new RegExp(r'^(#{1,6})\s(.*?)(?:\n+|$)'), 'lheading': new RegExp(r'^([^\n]+)\n *(=|-){2,} *(?:\n+|$)'), 'blockquote': new RegExp(r'^(>[^\n]+([^\n]+)*\n*)+'), 'ordered-list': new RegExp(r'^((\d+\.){1,6}\s[\s\S]+?)(?:\n[^\d]|$)'), 'unordered-list': new RegExp(r'^(([*+-]){1,6}\s[\s\S]+?)(?:\n[^*+-]|$)'), 'html': new RegExp(r'^ *(?:comment|closed|closing) *(?:\n{2,}|\s*$)'), 'def': new RegExp(r'^ *\[([^\]]+)\]: *<?([^\s>]+)>?(?: +["(]([^\n]+)[")])? *(?:\n+|$)'), 'paragraph': new RegExp(r'^(.*)\n'), 'ordered-item': new RegExp(r'^(\d+\.){1,6}\s.*\n', multiLine:true, caseSensitive: true), 'unordered-item': new RegExp(r'^([*+-]){1,6}\s.*\n', multiLine:true, caseSensitive: true), 'text': new RegExp(r'^[^\n]+')
};

/**
 * Normal Block Grammar
 */

Map<String, RegExp> blockNormal = _merge({
}, block);


String markdownToHtml(String src) {
  List<Token> tokens = Lexer.lex(src);
  return Parser.parse(tokens);
}

Map<String, RegExp> _merge(Map<String, RegExp> a, Map<String, RegExp>b, [Map<String, RegExp> c]) {
  Map<String, RegExp> result = new Map<String, RegExp>();
  result.addAll(a);
  result.addAll(b);
  if (c != null) {
    result.addAll(c);
  }
  return result;
}

class Lexer {
  List<Token> tokens = [];
  Map links = {
  };
  Map<String, RegExp> rules = blockNormal;
  Match cap;
  int level = 0;

  static List<Token> lex(String src) {
    Lexer lexer = new Lexer();
    return lexer._lex(src);
  }

  Lexer() {

  }

  List<Token> _lex(String src) {
    if (src == null) {
      src = '';
    }
    src = src.replaceAll(new RegExp('\r\n|\r'), '\n').replaceAll(new RegExp('\t'), '    ').replaceAll(new RegExp('\u00a0'), ' ').replaceAll(new RegExp('\u2424'), '\n');
    if (!src.endsWith('\n')) {
      src += '\n';
    }
    return _token(src, true);
  }

  List<Token> _token(String src, bool top) {
    var _src = src.replaceAll(new RegExp(r'^ +$'), '');
    while (_src.length > 0) {
      // newline
      if ((cap = this.rules['newline'].firstMatch(_src)) != null) {
        int lenghtOfFirstMatch = cap.end - cap.start;
        _src = _src.substring(lenghtOfFirstMatch);
        if (lenghtOfFirstMatch > 1) {
          this.tokens.add(new Token(TokenType.SPACE));
        }
      }

      // code
      if ((cap = this.rules['code'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        String text = cap.group(0).replaceAll(new RegExp('^ {4}'), '');
        this.tokens.add(new Token(TokenType.CODE, text));
        continue;
      }

      //fences
      if ((cap = this.rules['fences'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        this.tokens.add(new Token.code(TokenType.CODE, cap.group(2), cap.group(3)));
        continue;
      }

      // heading
      if (top && (cap = this.rules['heading'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        this.tokens.add(new Token(TokenType.HEADING, cap.group(2), cap.group(1).length));
        continue;
      }

      // lheading
      if (top && (cap = this.rules['lheading'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        String text = cap.group(1);
        int depth = cap[2] == '=' ? 1 : 2;
        this.tokens.add(new Token(TokenType.HEADING, text, depth));
        continue;
      }

      // hr
      if (top && (cap = this.rules['hr'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        this.tokens.add(new Token(TokenType.HR));
        continue;
      }

      // blockquote
      if (top && (cap = this.rules['blockquote'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        int blockLevel = new RegExp('(^>{0,6})').firstMatch(cap.group(0)).group(0).length;
        bool isBlockLevelIncreased = false;
        if (blockLevel > this.level) {
          this.tokens.add(new Token(TokenType.BLOCKQUOTE_START));
          this.level++;
          isBlockLevelIncreased = true;
        } else if (blockLevel < this.level) {
          this.tokens.add(new Token(TokenType.BLOCKQUOTE_END));
          this.level--;
        }
        String sub = cap.group(0).replaceFirst(new RegExp(r'^>{' + this.level.toString() + '} ?'), '');
        // Pass `top` to keep the current
        // "toplevel" state. This is exactly
        // how markdown.pl works.

        _token(sub, top);
        if (isBlockLevelIncreased && this.level > 0) {
          this.tokens.add(new Token(TokenType.BLOCKQUOTE_END));
          this.level--;
        }
        continue;
      }
      //TODO: Refactor ordered and unordered
      // unordered-list
      if ((cap = this.rules['unordered-list'].firstMatch(_src)) != null) {

        _src = _src.substring(this.rules['unordered-list'].firstMatch(_src).group(1).length);
        this.tokens.add(new Token.list(TokenType.LIST_START, false));

        // Get each item.
        List<Match> itemMatches = new List.from(this.rules['unordered-item'].allMatches(cap.group(0)));

        //TODO: very bad, this can be done with one regexp.
        List<String> items = _repairList(itemMatches, false);
        for (String item in items) {
          // Remove the list item's bullet
          // so it is seen as the next token.
          item = item.replaceFirst(new RegExp(r'^([*+-])'), '').replaceAll(new RegExp(r'\n([*+-])'), '\n').replaceFirst(new RegExp(r'^\s+'), '');


          this.tokens.add(new Token(TokenType.LIST_ITEM_START));
          // Recurse.
          _token(item, false);
          this.tokens.add(new Token(TokenType.LIST_ITEM_END));
        }

        this.tokens.add(new Token(TokenType.LIST_END));

        continue;
      }

      // ordered-list
      if ((cap = this.rules['ordered-list'].firstMatch(_src)) != null) {
        _src = _src.substring(this.rules['ordered-list'].firstMatch(_src).group(1).length);
        this.tokens.add(new Token.list(TokenType.LIST_START, true));
        // Get each item.
        List<Match> itemMatches = new List.from(this.rules['ordered-item'].allMatches(cap.group(0)));

        //TODO: very bad, this can be done with one regexp.
        List<String> items = _repairList(itemMatches, true);
        for (String item in items) {
          // Remove the list item's bullet
          // so it is seen as the next token.
          item = item.replaceFirst(new RegExp(r'^(\d+\.)'), '').replaceAll(new RegExp(r'\n(\d+\.)'), '\n').replaceFirst(new RegExp(r'^\s+'), '');


          this.tokens.add(new Token(TokenType.LIST_ITEM_START));

          // Recurse.
          _token(item, false);

          this.tokens.add(new Token(TokenType.LIST_ITEM_END));
        }

        this.tokens.add(new Token(TokenType.LIST_END));
        continue;
      }

      // top-level paragraph
      if (top && (cap = this.rules['paragraph'].firstMatch(_src)) != null) {
        _src = _src.substring(cap.group(0).length);
        String text = cap.group(1);
        this.tokens.add(new Token(TokenType.PARAGRAPH, text));
        continue;
      }

      // text
      if ((cap = this.rules['text'].firstMatch(_src)) != null) {
        // Top-level should never reach here.
        _src = _src.substring(cap.group(0).length);
        this.tokens.add(new Token(TokenType.TEXT, cap.group(0)));
        continue;
      }

      if (!_src.isEmpty) {
        throw new ArgumentError('Infinite loop on char: ' + src);
      }
    }
    return this.tokens;
  }

  List<String> _repairList(List<Match> listToRepair, bool ordered) {
    if (listToRepair.isEmpty) {
      return new List<String>();
    }
    List<String> result = new List<String>();
    int level = _getLevelOfElement(listToRepair.first.group(0), ordered);
    for (Match element in listToRepair) {
      int levelOfElement = _getLevelOfElement(element.group(0), ordered);
      if (level == levelOfElement) {
        result.add(element.group(0));
      } else if (level < levelOfElement) {
        String last = result.removeLast();
        last = last + element.group(0);
        result.add(last);
      }
    }
    return result;
  }

  int _getLevelOfElement(String element, bool ordered) {
    if (ordered) {
      String listMarker = new RegExp(r'^(\d+\.){1,6}\s').firstMatch(element).group(0);
      return new RegExp(r'\.').allMatches(listMarker).length;
    } else {
      String listMarker = new RegExp(r'^[*+-]{1,6}\s').firstMatch(element).group(0);
      return listMarker.length;
    }
  }
}

class Token {

  TokenType type;
  String text;
  String language;
  int depth;
  bool ordered;

  Token(this.type, [this.text, this.depth]) {

  }

  Token.list(this.type, this.ordered) {

  }

  Token.code(this.type, this.language, this.text) {

  }
}

class TokenType {
  final String _type;

  const TokenType._internal(this._type);

  static const TEXT = const TokenType._internal('text');
  static const SPACE = const TokenType._internal('space');
  static const HR = const TokenType._internal('hr');
  static const HEADING = const TokenType._internal('heading');
  static const CODE = const TokenType._internal('code');
  static const BLOCKQUOTE_START = const TokenType._internal('blockquote_start');
  static const BLOCKQUOTE_END = const TokenType._internal('blockquote_end');
  static const LIST_START = const TokenType._internal('list_start');
  static const LIST_END = const TokenType._internal('list_end');
  static const LIST_ITEM_START = const TokenType._internal('list_item_start');
  static const LIST_ITEM_END = const TokenType._internal('list_item_end');
  static const ITEM_START = const TokenType._internal('item_start');
  static const PARAGRAPH = const TokenType._internal('paragraph');
  static const HTML = const TokenType._internal('html');
  static const TABLE = const TokenType._internal('table');
}

class Parser {
  List<Token> tokens;
  Token token;
  InlineLexer inline;
  Renderer renderer;

  static String parse(List<Token> tokens) {
    Parser parser = new Parser();
    parser.renderer = new Renderer();
    parser.inline = new InlineLexer();
    return parser._parse(tokens);
  }

  String _parse(List<Token> tokens) {
    this.tokens = new List.from(tokens.reversed);
    String out = '';
    while (_hasNext()) {
      _next();
      out += _tok();
    }
    return out;
  }

  Token _next() {
    if (tokens.isEmpty) {
      return null;
    } else {
      this.token = tokens.removeLast();
      return token;
    }
  }

  Token _peek() {
    return tokens.last;
  }

  bool _hasNext() {
    return tokens.isNotEmpty;
  }

  String _tok() {
    switch (this.token.type) {
      case TokenType.SPACE:
        return '';
      case TokenType.HEADING:
        return this.renderer.heading(this.inline.output(this.token.text), this.token.depth);
      case TokenType.CODE:
        return this.renderer.code(escape(this.token.text, true), this.token.language);
      case TokenType.HR:
        return this.renderer.hr();
      case TokenType.BLOCKQUOTE_START:
        String body = '';
        while (_hasNext() && _next().type != TokenType.BLOCKQUOTE_END) {
          body += _tok();
        }
        return this.renderer.blockquote(body);
      case TokenType.LIST_START:
        String body = '';
        bool ordered = this.token.ordered;
        while (_next().type != TokenType.LIST_END) {
          body += _tok();
        }
        return this.renderer.list(body, ordered);
      case TokenType.LIST_ITEM_START:
        String body = '';
        while (_next().type != TokenType.LIST_ITEM_END) {
          body += this.token.type == TokenType.TEXT ? _parseText() : _tok();
        }
        return this.renderer.listItem(body);
      case TokenType.PARAGRAPH:
        return this.renderer.paragraph(this.inline.output(this.token.text));
      case TokenType.TEXT:
        return this.renderer.paragraph(_parseText());
      default:
        throw new ArgumentError(this.token.type._type + " is not a valid type");
    }
  }

  /**
   * Parse Text Tokens
   */

  String _parseText() {
    var body = this.token.text;

    while (_hasNext() && _peek().type == TokenType.TEXT) {
      body += '\n' + _next().text;
    }

    return this.inline.output(body);
  }
}

class Renderer {
  String code(String code, String language) {
    if (language == null || language.isEmpty) {
      return '<pre><code>$code\n</code></pre>';
    } else {
      return '<pre class=\"$language\"><code>$code\n</code></pre>';
    }
  }

  String blockquote(quote) {
    return '<blockquote>\n' + quote + '</blockquote>\n';
  }

  String html(html) {
    return html;
  }

  String heading(String text, int level) {
    return '<h' + level.toString() + '>' + text + '</h' + level.toString() + '>\n';
  }

  String hr() {
    return '<hr>\n';
  }

  String list(body, ordered) {
    var type = ordered ? 'ol' : 'ul';
    return '<' + type + '>\n' + body + '</' + type + '>\n';
  }

  String listItem(text) {
    return '<li>' + text + '</li>\n';
  }

  String paragraph(text) {
    return '<p>' + text + '</p>\n';
  }

  String tablen(header, body) {
    return '<table>\n' + '<thead>\n' + header + '</thead>\n' + '<tbody>\n' + body + '</tbody>\n' + '</table>\n';
  }

  String tablerow(content) {
    return '<tr>\n' + content + '</tr>\n';
  }

  String tablecell(String content, flags) {
    var type = flags.header ? 'th' : 'td';
    var tag = flags.align ? '<' + type + ' style="text-align:' + flags.align + '">' : '<' + type + '>';
    return tag + content + '</' + type + '>\n';
  }

  // span level renderer

  String strong(String text) {
    return '<strong>' + text + '</strong>';
  }

  String em(String text) {
    return '<em>' + text + '</em>';
  }

  String del(String text) {
    return '<del>' + text + '</del> ';
  }

  String codespan(String text) {
    return '<code>' + text + '</code>';
  }

  String br() {
    return '<br>';
  }

  String link(String href, String title, String text) {
    var out = '<a href="' + href + '"';
    if (title.isNotEmpty) {
      out += ' title="' + title + '"';
    }
    out += '>' + text + '</a>';
    return out;
  }

  String image(String href, String title, String text) {
    var out = '<img src="' + href + '" alt="' + text + '"';
    if (title.isNotEmpty) {
      out += ' title="' + title + '"';
    }
    out += '>';
    return out;
  }
}

Map<String, RegExp> inline = {
    'escape': new RegExp(r'^\\([\\`*{}\[\]()#+\-.!_>])'), 'autolink': new RegExp(r'^<([^ >]+(@|:\/)[^ >]+)>'), //'url': noop,
    'tag': new RegExp('^<!--[\\s\\S]*?-->|^<\\/?\\w+(?:"[^"]*"|\'[^\']*\'|[^\'">])*?>'), 'link': new RegExp('^!?\\[((?:\\[[^\\]]*\\]|[^\\[\\]]|\\](?=[^\\[]*\\]))*)\\]\\(\\s*<?([\\s\\S]*?)>?(?:\\s+[\\\'\"]([\\s\\S]*?)[\\\'\"])?\\s*\\)'), 'reflink': new RegExp(r'^!?\[((?:\[[^\]]*\]|[^\[\]]|\](?=[^\[]*\]))*)\]\s*\[([^\]]*)\]'), 'nolink': new RegExp(r'^!?\[((?:\[[^\]]*\]|[^\[\]])*)\]'), 'strong': new RegExp(r'^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)'), 'em': new RegExp(r'^\b_((?:__|[\s\S])+?)_\b|^\*((?:\*\*|[\s\S])+?)\*(?!\*)'), 'del': new RegExp(r'^-((?:--|[\s\S])+?)(-(?: |$))(?!-)'), 'code': new RegExp(r'^(`+)\s*([\s\S]*?[^`])\s*\1(?!`)'), 'br': new RegExp(r'^ {2,}\n(?!\s*$)'), 'text': new RegExp(r'^[\s\S]+?(?=(\\|<|!|\[|_|\*|`|[^ ]-|-[^ \,])| {2,}\n|$)')
};

Map<String, RegExp> inlineNormal = _merge({
}, inline);

class InlineLexer {
  Map<String, RegExp> rules = inlineNormal;
  Renderer renderer = new Renderer();
  bool inLink;

  String output(String src) {
    var out = '', link, href;
    String text;
    Match cap;


    while (src.length > 0) {
      // escape
      if ((cap = this.rules['escape'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += cap.group(1);
        continue;
      }

      // link
      if ((cap = this.rules['link'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        this.inLink = true;
        out += this.outputLink(cap.group(0), cap.group(2), cap.group(3), cap.group(1));
        this.inLink = false;
        continue;
      }

      // strong
      if ((cap = this.rules['strong'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += this.renderer.strong(output(cap.group(2) != null ? cap.group(2) : cap.group(1)));
        continue;
      }

      // em
      if ((cap = this.rules['em'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += this.renderer.em(this.output(cap.group(2) != null ? cap.group(2) : cap.group(1)));
        continue;
      }

      // del
      if ((cap = this.rules['del'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += this.renderer.del(this.output(cap.group(1)));
        continue;
      }

      // code
      if ((cap = this.rules['code'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += this.renderer.codespan(escape(cap.group(2), true));
        continue;
      }

      // br
      if ((cap = this.rules['br'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += this.renderer.br();
        continue;
      }

      // text
      if ((cap = this.rules['text'].firstMatch(src)) != null) {
        src = src.substring(cap.group(0).length);
        out += escape(cap.group(0), false);
        continue;
      }

      if (src.length > 0) {
        throw new ArgumentError('Infinite loop on char: ' + src);
      }
    }
    return out;
  }

  String outputLink(String full, String href, String title, String text) {
    href = escape(href, false);
    title = title != null ? escape(title, false) : '';
    if (full[0] != '!') {
      return this.renderer.link(href, title, this.output(text));
    } else {
      return this.renderer.image(href, title, escape(text, false));
    }
  }
}


/**
 * Helpers
 */

String escape(String html, bool encode) {
  return html.replaceAll(!encode ? new RegExp('/&(?!#?\w+;)') : new RegExp('&'), '&amp;').replaceAll(new RegExp('<'), '&lt;').replaceAll(new RegExp('>'), '&gt;').replaceAll(new RegExp('"'), '&quot;').replaceAll(new RegExp('\''), '&#39;');
}
