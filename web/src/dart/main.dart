import 'editor/editor.dart';
import 'preview/preview.dart';
import 'menu/menuBottom.dart';
import 'menu/menuLeft.dart';
import 'menu/menuTop.dart';
import 'eventbus/eventbus.dart';
import 'model/model.dart';
import 'dart:async';
import 'dart:html';
import 'i18n/i18n.dart' as I18n;

final List<String> SUPPORTED_BROWSERS = [''];

BottomMenuViewPresenter bottomMenuViewPresenter;
LeftMenuViewPresenter leftMenuViewPresenter;
TopMenuViewPresenter rightMenuViewPresenter;
PreviewViewPresenter previewPresenter;
EditorViewPresenter editorViewPresenter;

EventBus bus;
Model model;
Element dialogContainer = querySelector('#dialog-container');

void main() {
  if(_isBrowserSupported()){
    _init();
  }else{
    print('Scheiße');
  }
}

void _init(){
  bus = new EventBus();
  I18n.language = window.navigator.language == null ? 'en' : window.navigator.language;
  model = new Model(bus);
  previewPresenter = new PreviewViewPresenter(new PreviewViewImpl(), bus, model);
  editorViewPresenter = new EditorViewPresenter(new EditorViewImpl(), bus, model);
  bottomMenuViewPresenter = new BottomMenuViewPresenter(new BottomMenuViewImpl(), bus, model);
  leftMenuViewPresenter = new LeftMenuViewPresenter(new LeftMenuViewImpl(), bus, model);
  rightMenuViewPresenter = new TopMenuViewPresenter(new TopMenuViewImpl(), bus);
  window.onResize.listen((Event event) => bus.fire(windowResizeEvent));
  document.onFullscreenChange.listen((Event event) {
    new Timer(new Duration(seconds:1), () => bus.fire(windowResizeEvent));
  });
  window.onMouseMove.listen((Event event) => bus.fire(mouseMoveEvent));
  window.onClick.listen((Event event) => _handleWindowClick(event.target));
  bus.on(showHtmlPreviewEvent).listen((bool show) {
    if (show) {
      querySelector('#main').style.justifyContent = 'flex-start';
    } else {
      querySelector('#main').style.justifyContent = 'center';
    }
  });
  bus.on(themeChangedEvent).listen((Theme theme) {
    querySelector('head link[theme]').setAttribute('href', theme.cssFile);
  });
  window.onKeyDown.listen((KeyboardEvent e) {
    if (e.keyCode == KeyCode.S && e.ctrlKey && e.altKey) {
      bus.fire(saveDocumentEvent);
      e.preventDefault();
    } else if (e.keyCode == KeyCode.C && e.ctrlKey && e.altKey) {
      bus.fire(copyDocumentEvent);
      e.preventDefault();
    } else if (e.keyCode == KeyCode.R && e.ctrlKey && e.altKey) {
      bus.fire(deleteDocumentEvent);
      e.preventDefault();
    } else if (e.keyCode == KeyCode.N && e.ctrlKey && e.altKey) {
      bus.fire(newDocumentEvent);
      e.preventDefault();
    }
  });
  model.init();
  _translate();
}

bool _isBrowserSupported(){
  return true;
}

void _translate() {
  ElementList elements = querySelectorAll('[i18n]');
  for (Element element in elements) {
    String key = element.getAttribute('i18n');
    element.firstChild.text = I18n.translate(key);
  }
  ElementList inputElements = querySelectorAll('[i18n-placeholder]');
  for (Element element in inputElements) {
    String key = element.getAttribute('i18n-placeholder');
    element.setAttribute('placeholder', I18n.translate(key));
  }
}

void _handleWindowClick(Element element) {
  if (_isBackgroundElement(element)) {
    bus.fire(windowClickEvent, element);
  }
}


bool _isBackgroundElement(Element element) {
  if (_elementOrParentHasId(element, 'main') || _elementOrParentHasId(element, 'menu-bottom') || element.id == 'menu-left') {
    return true;
  } else {
    return false;
  }
}

bool _elementOrParentHasId(Element element, String id) {
  Element currentElement = element;
  do {
    if (currentElement.id == id) {
      return true;
    }
    currentElement = currentElement.parent;
  } while (currentElement != null);
  return false;
}
