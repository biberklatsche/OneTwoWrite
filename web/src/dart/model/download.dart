library download;

import 'dart:html';

void download(String filename, String content) {
  AnchorElement link = document.createElement('a');
  Blob blob = new Blob([content], 'text');
  String url = Url.createObjectUrlFromBlob(blob);
  link.setAttribute('href', url);
  link.setAttribute('type', 'application/octet-stream');
  link.setAttribute('download', filename);
  document.body.append(link);
  link.click();
  link.remove();
}