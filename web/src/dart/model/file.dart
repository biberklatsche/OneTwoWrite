library file;

import 'model.dart';
import 'dart:html';
import 'dart:indexed_db' as idb;
import 'dart:async';

class FileHandler {
  static final String _ONE_TWO_WRITE_DB = "oneTwoWrite";
  static final String _ONE_TWO_WRITE_STORE = "documents";
  static final String _ONE_TWO_WRITE_SETTINGS_STORE = "settings";

  idb.Database _db;
  int _version = 3;

  Future open() {
    return window.indexedDB.open(_ONE_TWO_WRITE_DB, version: _version, onUpgradeNeeded: _onUpgradeNeeded).then(_onDbOpened).catchError(_onError);
  }

  void _onError(e) {
    window.location.href = "./error.html";
  }

  void _onDbOpened(idb.Database db) {
    _db = db;
  }

  void _onUpgradeNeeded(idb.VersionChangeEvent e) {
    idb.Database db = (e.target as idb.OpenDBRequest).result;
    if (!db.objectStoreNames.contains(_ONE_TWO_WRITE_STORE)) {
      db.createObjectStore(_ONE_TWO_WRITE_STORE, keyPath: 'name');
    }
    if (!db.objectStoreNames.contains(_ONE_TWO_WRITE_SETTINGS_STORE)) {
      db.createObjectStore(_ONE_TWO_WRITE_SETTINGS_STORE, keyPath: 'settings');
    }

  }

  Future saveSettings(Settings settings) {
    var trans = _db.transaction(_ONE_TWO_WRITE_SETTINGS_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_SETTINGS_STORE);
    return store.put({
        'font-size': settings.fontSize.toString(), 'show-preview': settings.showPreview, 'preview-width': settings.previewWidth, 'preview-height': settings.previewHeight, 'preview-right': settings.previewRight, 'preview-bottom': settings.previewBottom, 'auto-save': settings.autoSafe, 'theme': settings.theme.toString(), 'last-edited-document': settings.lastOpenedDocument, 'settings': 'settings'
    }).then((_) => print('settings saved!')).catchError((e) => _onError);
  }

  Future<Settings> getSettings() {
    var trans = _db.transaction(_ONE_TWO_WRITE_SETTINGS_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_SETTINGS_STORE);
    var content;
    return store.openCursor(autoAdvance:true).firstWhere((cursor) => cursor.key == 'settings', defaultValue: () => new Settings()).then((cursor) {
      if (cursor is Settings) {
        return cursor;
      } else {
        Settings settings = new Settings();
        settings.fontSize = FontSize.fromString(cursor.value['font-size']);
        settings.theme = Theme.fromString(cursor.value['theme']);
        settings.showPreview = cursor.value['show-preview'];
        settings.autoSafe = cursor.value['auto-save'];
        settings.lastOpenedDocument = cursor.value['last-edited-document'];
        settings.previewWidth = cursor.value['preview-width'];
        settings.previewHeight = cursor.value['preview-height'];
        settings.previewBottom = cursor.value['preview-bottom'];
        settings.previewRight = cursor.value['preview-right'];
        return settings;
      }
    });
  }

  Future saveDocument(EditorDocument document) {
    var trans = _db.transaction(_ONE_TWO_WRITE_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_STORE);
    return store.put({
        'text': document.text, 'preview': document.preview, 'name': document.name, 'words': document.words, 'chars': document.chars, 'readingTimeInSeconds': document.readingTimeInSeconds, 'timeStamp': new DateTime.now().millisecondsSinceEpoch
    }).then((_) => print('document saved!')).catchError((e) => _onError);
  }

  Future deleteDocument(String name) {
    var trans = _db.transaction(_ONE_TWO_WRITE_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_STORE);
    var request = store.delete(name);
    return request.then((_) => print('document deleted!'), onError: _onError);
  }

  Future<int> countDocuments() {
    var trans = _db.transaction(_ONE_TWO_WRITE_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_STORE);
    return store.count();
  }

  Future<List<String>> getAllDocumentNames() {
    var trans = _db.transaction(_ONE_TWO_WRITE_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_STORE);
    List<String> documentNames = new List();
    return store.openCursor(autoAdvance:true).forEach((content) {
      if (content.key != Model.DUMMY_DOCUMENT_NAME) {
        documentNames.add(content.key);
      }
    }).then((_) {
      return documentNames;
    }, onError: _onError);
  }

  Future<EditorDocument> getDocument(String name) {
    var trans = _db.transaction(_ONE_TWO_WRITE_STORE, 'readwrite');
    var store = trans.objectStore(_ONE_TWO_WRITE_STORE);
    var content;
    return store.openCursor(autoAdvance:true).firstWhere((cursor) {
      return cursor.key == name;
    }).then((cursor) {
      EditorDocument doc = new EditorDocument();
      doc.name = cursor.value['name'];
      doc.text = cursor.value['text'];
      doc.preview = cursor.value['preview'];
      doc.chars = cursor.value['chars'] == null ? 0 : cursor.value['chars'];
      doc.words = cursor.value['words'] == null ? 0 : cursor.value['words'];
      doc.readingTimeInSeconds = cursor.value['readingTimeInSeconds'] == null ? 0 : cursor.value['readingTimeInSeconds'];
      doc.timeStamp = cursor.value['timeStamp'];
      return doc;
    }, onError: _onError);
  }
}