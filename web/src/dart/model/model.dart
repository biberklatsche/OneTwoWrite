library model;
import 'file.dart';
import '../eventbus/eventbus.dart';
import '../i18n/i18n.dart' as I18n;
import 'dart:async';
import '../parser/marked/src/marked.dart' as Mark;
import '../utils/web/webUtils.dart' as Utils;

class Model {

  bool isNewDocument = true;
  bool _edited = false;
  bool _focusMode = false;
  String _currentDocumentName;
  Settings _settings;

  static String DUMMY_DOCUMENT_NAME = '{{default}}';

  FileHandler _fileHandler;
  EventBus _bus;

  Model(this._bus) {
    _fileHandler = new FileHandler();
  }

  void init() {
    _fileHandler.open().then((_) {
      _fileHandler.getSettings().then((Settings settings) {
        _settings = settings;
        _bus.fire(themeChangedEvent, _settings.theme);
        _bus.fire(fontSizeChangeEvent, _settings.fontSize);
        _bus.fire(showHtmlPreviewEvent, _settings.showPreview);

        openDefaultDocument() {
          isNewDocument = true;
          isEdited = false;
          currentDocumentName = I18n.translate(I18n.DEFAULT_DOCUMENT_NAME);
          _bus.fire(documentNameChangedEvent, currentDocumentName);
        }

        if (_settings.lastOpenedDocument.isNotEmpty) {
          _fileHandler.getDocument(_settings.lastOpenedDocument).then((EditorDocument doc) {
            if (doc != null) {
              isNewDocument = doc.name == DUMMY_DOCUMENT_NAME;
              isEdited = false;
              if (isNewDocument) {
                currentDocumentName = I18n.translate(I18n.DEFAULT_DOCUMENT_NAME);
                _bus.fire(loadDocumentEvent, DUMMY_DOCUMENT_NAME);
              } else {
                currentDocumentName = _settings.lastOpenedDocument;
                _bus.fire(loadDocumentEvent, currentDocumentName);
              }
              _bus.fire(documentNameChangedEvent, currentDocumentName);

            } else {
              openDefaultDocument();
            }
          });
        } else {
          openDefaultDocument();
        }
      });
    });
  }


  Future<EditorDocument> getDocument(String documentName) {
    return _fileHandler.getDocument(documentName);
  }

  Future<List<String>> getAllDocumentNames() {
    return _fileHandler.getAllDocumentNames();
  }

  Future<int> countDocuments() {
    return _fileHandler.countDocuments();
  }

  Future deleteDocument(String name) {
    if (_settings.lastOpenedDocument == name) {
      _settings.lastOpenedDocument = '';
    }
    return _fileHandler.deleteDocument(name);
  }

  Future saveDocument(EditorDocument document) {
    isEdited = false;
    if (isNewDocument) {
      document.name = DUMMY_DOCUMENT_NAME;
    }
    return _fileHandler.saveDocument(document);
  }

  Future saveSettings(Settings settings) {
    _settings = settings;
    return _fileHandler.saveSettings(settings);
  }

  Settings getSettings() {
    return _settings;
  }

  String convertMarkdownToHtml(String title, String markdown) {
    return Utils.calcHtml(title, markdown);
  }

  convertMarkdownToPDF(String title, String markdown) {
    return Utils.calcPdf(title, markdown);
  }

  bool get isEdited => _edited;

  void set isEdited(bool edited) {
    this._edited = edited;
    _bus.fire(documentEditedEvent, edited);
  }

  bool get isOnFocusMode => _focusMode;

  void set isOnFocusMode(bool focusMode) {
    this._focusMode = focusMode;
    _bus.fire(focusModeChangedEvent, focusMode);
  }

  String get currentDocumentName => _currentDocumentName;

  void set currentDocumentName(String value) {
    this._currentDocumentName = value;
    if (!isNewDocument) {
      _settings.lastOpenedDocument = value;
      _fileHandler.saveSettings(_settings);
    } else {
      _settings.lastOpenedDocument = Model.DUMMY_DOCUMENT_NAME;
      _fileHandler.saveSettings(_settings);
    }
    _bus.fire(documentNameChangedEvent, value);
  }
}

class Settings {
  FontSize fontSize = FontSize.AUTO;
  Theme theme = Theme.Light;
  bool autoSafe = true;
  bool showPreview = false;
  int previewRight = 50;
  int previewBottom = 50;
  int previewWidth = 300;
  int previewHeight = 300;
  String _lastOpenedDocument = '';

  String get lastOpenedDocument => _lastOpenedDocument;

  void set lastOpenedDocument(String value) {
    this._lastOpenedDocument = value == null ? '' : value;
  }
}

class FontSize {
  final String _size;

  const FontSize._internal(this._size);

  toString() => 'FontSize.$_size';

  static FontSize fromString(String size) {
    switch (size) {
      case 'FontSize.small':
        return FontSize.SMALL;
      case 'FontSize.medium':
        return FontSize.MEDIUM;
      case 'FontSize.large':
        return FontSize.LARGE;
      default:
        return FontSize.AUTO;
    }
  }

  static const AUTO = const FontSize._internal('auto');
  static const SMALL = const FontSize._internal('small');
  static const MEDIUM = const FontSize._internal('medium');
  static const LARGE = const FontSize._internal('large');
}

class Theme {
  final String _name;
  final String cssFile;
  final String editorTheme;

  const Theme._internal(this._name, this.cssFile, this.editorTheme);

  toString() => 'Theme.$_name';

  static Theme fromString(String name) {
    switch (name) {
      case 'Theme.dark':
        return Theme.Dark;
      default:
        return Theme.Light;
    }
  }

  static const Light = const Theme._internal('light', 'src/css/light.css', 'ace/theme/writer-light');
  static const Dark = const Theme._internal('dark', 'src/css/dark.css', 'ace/theme/writer-dark');
}

class EditorDocument {
  String text;
  String preview;
  String name;
  int words;
  int chars;
  int readingTimeInSeconds;
  int timeStamp;
}

class DownloadDocumentType {
  final _type;

  const DownloadDocumentType._internal(this._type);

  toString() => 'DocumentType.$_type';

  static DownloadDocumentType fromString(String type) {
    switch (type) {
      case 'DocumentType.txt':
        return DownloadDocumentType.TXT;
      case 'DocumentType.pdf':
        return DownloadDocumentType.PDF;
      case 'DocumentType.html':
        return DownloadDocumentType.HTML;
      default:
        return DownloadDocumentType.MD;
    }
  }

  static const MD = const DownloadDocumentType._internal('md');
  static const PDF = const DownloadDocumentType._internal('pdf');
  static const TXT = const DownloadDocumentType._internal('txt');
  static const HTML = const DownloadDocumentType._internal('html');
}
