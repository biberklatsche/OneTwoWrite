library preview;
import '../eventbus/eventbus.dart';
import '../model/model.dart';
import 'dart:html';

part '../preview/previewImpl.dart';

abstract class PreviewViewListener {
  void dragStart(int mouseX, int mouseY, int offsetLeft, int offsetTop);

  void mouseMove(int x, int y);

  void dragStop();

  void setMoveDragMode();

  void setResizeDragMode();

  void closePreview();
}

abstract class PreviewView {
  void setListener(PreviewViewListener listener);

  void setHtmlPreview(String html);

  void showPreview(bool show);

  void setPosition(int x, int y);

  void setPositionRightBottom(int right, int bottom);

  void setSize(int width, int height);

  void setSizeAndSavePosition(int width, int height);

  void resize();

  int getWidth();

  int getHeight();

  int getRight();

  int getBottom();
}