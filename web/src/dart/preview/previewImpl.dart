part of preview;

class PreviewViewPresenter implements PreviewViewListener {
  final EventBus _bus;
  final PreviewView _view;
  final Model _model;

  PreviewViewPresenter(this._view, this._bus, this._model) {
    _view.setListener(this);
    _bus.on(showHtmlPreviewEvent).listen((bool show) {
      _view.showPreview(show);
      if (show) {
        int bottom = _model.getSettings().previewBottom;
        int right = _model.getSettings().previewRight;
        int height = _model.getSettings().previewHeight;
        int width = _model.getSettings().previewWidth;
        _view.setPositionRightBottom(right, bottom);
        _view.setSize(width, height);
      }
    });
    _bus.on(htmlPreviewCalculatedEvent).listen((String html) {
      _view.setHtmlPreview(html);
    });
    _bus.on(windowResizeEvent).listen((_) {
      _view.resize();
    });
  }

  int containerX = 0;
  int containerY = 0;

  int mouseX = 0;
  int mouseY = 0;

  bool isInMoveDragMode = false;
  bool isInResizeDragMode = false;

  void mouseMove(int x, int y) {
    if (isInMoveDragMode) {
      int elemX = mouseX - containerX;
      int elemY = mouseY - containerY;
      int newX = x - elemX;
      int newY = y - elemY;
      _view.setPosition(newX, newY);
    }
    if (isInResizeDragMode) {
      int width = x - containerX;
      int height = y - containerY;
      _view.setSizeAndSavePosition(width < 300 ? 300 : width, height < 300 ? 300 : height);
    }

  }

  void dragStop() {
    if (isInMoveDragMode || isInResizeDragMode) {
      isInMoveDragMode = false;
      isInResizeDragMode = false;
      Settings settings = _model.getSettings();
      settings.previewWidth = _view.getWidth();
      settings.previewHeight = _view.getHeight();
      settings.previewRight = _view.getRight();
      settings.previewBottom = _view.getBottom();
      _model.saveSettings(settings);
    }
  }

  void setResizeDragMode() {
    isInResizeDragMode = true;
  }

  void setMoveDragMode() {
    isInMoveDragMode = true;
  }

  void dragStart(int mouseX, int mouseY, int offsetLeft, int offsetTop) {
    this.mouseX = mouseX;
    this.mouseY = mouseY;
    this.containerX = offsetLeft;
    this.containerY = offsetTop;
  }

  void closePreview() {
    Settings settings = _model.getSettings();
    settings.showPreview = false;
    _model.saveSettings(settings);
    _bus.fire(showHtmlPreviewEvent, false);
  }
}

class PreviewViewImpl extends PreviewView {

  Element _previewContainer;
  Element _preview;
  Element _previewHeading;
  Element _previewResize;
  PreviewViewListener _listener;

  PreviewViewImpl() {
    _init();
  }

  void _init() {
    querySelector('#button-close-preview').onClick.listen((Event event) => _onCloseButtonClick());
    _previewContainer = querySelector("#html-preview-container");
    _preview = querySelector("#html-preview");
    _previewHeading = querySelector("#html-preview-container-heading");
    _previewResize = querySelector("#html-preview-resize");
    _previewHeading.onMouseDown.listen((MouseEvent event) => _onMoveDragStart(event));
    _previewResize.onMouseDown.listen((MouseEvent event) => _onResizeDragStart(event));
    document.onMouseMove.listen((MouseEvent event) => _onMouseMove(event));
    document.onMouseUp.listen((MouseEvent event) => _onDragStop(event));
  }

  void setListener(PreviewViewListener listener) {
    this._listener = listener;
  }

  void showPreview(bool show) {
    if (show) {
      _previewContainer.classes.remove('display-none');
    } else {
      _previewContainer.classes.add('display-none');
    }
  }

  _onCloseButtonClick() {
    _listener.closePreview();
  }

  void setHtmlPreview(String html) {
    _preview.children.clear();
    _preview.appendHtml(html);
  }

  void setPosition(int x, int y) {
    int right = window.innerWidth - x - _previewContainer.clientWidth;
    if (right + _previewContainer.clientWidth >= 30 && x >= 0) {
      _previewContainer.style.right = right.toString() + 'px';
    }
    int bottom = window.innerHeight - y - _previewContainer.clientHeight;
    if (bottom + _previewContainer.clientHeight >= 30 && y >= 0) {
      _previewContainer.style.bottom = bottom.toString() + 'px';
    }
  }

  void setPositionRightBottom(int right, int bottom) {
    _previewContainer.style.right = right.toString() + 'px';
    _previewContainer.style.bottom = bottom.toString() + 'px';
  }

  void setSizeAndSavePosition(int width, int height) {
    int right = getRight();
    int bottom = getBottom();
    int divWidth = width - _previewContainer.clientWidth;
    int divHeight = height - _previewContainer.clientHeight;
    _previewContainer.style.right = (right - divWidth - 2).toString() + 'px';
    _previewContainer.style.bottom = (bottom - divHeight - 2).toString() + 'px';
    setSize(width, height);
  }

  void setSize(int width, int height) {
    int right = getRight();
    int bottom = getBottom();
    int maxWidth = window.innerWidth;
    int maxHeight = window.innerHeight;
    if (maxWidth < right + width) {
      width = maxWidth - right;
    }
    if (maxHeight < bottom + height) {
      height = maxHeight - bottom;
    }
    _previewContainer.style.width = width.toString() + 'px';
    _previewContainer.style.height = height.toString() + 'px';
  }

  void _onMoveDragStart(MouseEvent event) {
    int x = event.client.x - _previewContainer.offsetLeft;
    int y = event.client.y - _previewContainer.offsetTop;
    _listener.setMoveDragMode();
    _listener.dragStart(event.client.x, event.client.y, _previewContainer.offsetLeft, _previewContainer.offsetTop);
  }

  void _onResizeDragStart(MouseEvent event) {
    _listener.setResizeDragMode();
    _listener.dragStart(event.client.x, event.client.y, _previewContainer.offsetLeft, _previewContainer.offsetTop);
  }

  void _onDragStop(MouseEvent event) {
    _listener.dragStop();
  }


  void _onMouseMove(MouseEvent event) {
    int x = event.client.x;
    int y = event.client.y;
    _listener.mouseMove(x, y);
  }

  int getWidth() {
    return _previewContainer.clientWidth;
  }

  int getHeight() {
    return _previewContainer.clientHeight;
  }

  int getRight() {
    return window.innerWidth - (_previewContainer.offsetLeft + _previewContainer.clientWidth);
  }

  int getBottom() {
    return window.innerHeight - (_previewContainer.offsetTop + _previewContainer.clientHeight);
  }

  void resize() {
    setSize(_previewContainer.clientWidth, _previewContainer.clientHeight);
  }
}

