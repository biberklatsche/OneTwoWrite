library editor;

import 'dart:html';
import '../eventbus/eventbus.dart';
import '../model/model.dart';
import '../utils/utils.dart' as Utils;
import '../model/download.dart' as DownloadUtil;
import '../dialog/dialog.dart' as Dialog;
import '../i18n/i18n.dart' as I18n;
import 'dart:js';
import 'dart:convert';
import 'dart:isolate';
import 'dart:async';


part '../editor/editorImpl.dart';

abstract class EditorViewListener {
  void headerLineHasFocus(int headerType);

  void unorderedListLineHasFocus();

  void orderedListLineHasFocus();

  void blockquoteLineHasFocus();

  void normalLineHasFocus();

  void strikeRangeHasFocus();

  void boldRangeHasFocus();

  void italicRangeHasFocus();

  void normalRangeHasFocus();

  void keyPressed();

  void documentEdited();

  bool isOnFocusMode();
}

abstract class EditorView {
  void setListener(EditorViewListener listener);

  void setContent(String content);

  String getContent();

  void enableFocusMode(bool enable);

  void removeLineMarker();

  void addHeaderLineMarker(int headerType);

  void addOrderedListLineMarker();

  void addUnorderedListLineMarker();

  void addBlockquoteLineMarker();

  void removeItalicInlineMarker();

  void removeBoldInlineMarker();

  void removeStrikeInlineMarker();

  void addItalicInlineMarker();

  void addBoldInlineMarker();

  void addStrikeInlineMarker();

  void showScrollbar();

  void focus();

  void resize();

  Future<bool> searchInDocument(String searchString);

  void searchNextInDocument();

  void searchPreviousInDocument();

  void replaceInDocument(String replaceString);

  void replaceAllInDocument(String replaceString);

  void clearSelection();

  void centerSelection();

  void setFontSizeSmall();

  void setFontSizeMedium();

  void setFontSizeLarge();

  void setTheme(Theme theme);
}
