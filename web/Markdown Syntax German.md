# Markdown Syntax Hilfe
Diese Seite basiert auf der Syntaxhilfe von [iA Writer](http://support.iawriter.com/help/kb/general-questions/markdown-syntax-reference-guide).
**OneTwoWrite** unterstützt einen Teil der  Markdown-Auszeichnugs-Sprache. Dies ermöglicht dir die Formatierung eines Texts mit ein paar einfachen Ausdrücken wie  \**kursiv*\* and \*\***fett**\*\*. 
 
- - -
## Überschriften
```
# Titel = Level 1 Überschrift
## Titel = Level 2 Überschrift
### Titel = Level 3 Überschrift
```
…bis zu sechs Level 
 
- - - 
## Kursiv und Fett
**Kursiv:** 
`*Beispiel*` oder `_Beispiel_`
**Fett:** 
`**Beispiel**` oder `__Beispiel__`
**Kursiv und Fett:** 
`***Beispiel***` oder `___Beispiel___`
**Gestrichen:** 
`-Beispiel-`

- - -
## Listen
**Nummerierte Listen:** Tippe `1.` und dann ein Leerzeichen
```
1. Listenelement
2. Listenelement
3. Listenelement
```
Jede Nummer (gefolgt von einem Punkt und einem Leerzeichen) kann genutzt werden — Listenelemente werden beim Exportieren von 1 aufwärts geordnet.

**Punktierte Listen:** Tippe `*`, `-` or `+` und dann ein Leerzeichen 
Erstelle eine punktierte Liste mit einem Sternchen- (*), Minus- (-), oder Plus-Zeichen (+), gefolgt von einem Leerzeichen. Zum Beipiel:
```
* Listenelement
* Listenelement
* Listenelement
```
**Geschachtelte Listen:**
Du kannst auch Listen bis zu sechs Level tief schachtel. Zum Beipiel:
```
* Erstes Level
** Zweites Level
...
****** Sechstes Level

1. Erstes Level
1.1. Zweites Level
...
1.1.1.1.1.1. Sechstes Level
```

- - - 
## Zitate 
Tippe `>` und dann ein Leerzeichen. Zum Beipiel:
```
> A quoted paragraph
>> A quoted paragraph inside a quotation
```

- - - 
## Links
Erzeuge einen Link indem du den Linktext mit eckigen Klammern umschließt, gefolgt von der URL in runden Klammern. Zum Beispiel:
```
[Das ist der Text des Links](http://beispiel.de/)
```

- - -
## Code
Du kannst Codezeilen erzeugen indem du den Quellcode in Hochkommas setzt (\`Quellcode\`).
```
Das ist normaler Text. `Das ist Quellcode` und das wieder normal Text.
```
oder markiere einen ganzen Block als Quellcode, indem du am Anfang und Ende des Blocks drei Hochkommas setzt. Zum Beispiel:
\`\`\`
    Quellcode-Block
\`\`\` 

Beachte, dass Formatierungszeichen (wie \_Unterstrich\_) in Code-Blöcken ignoriert werden.

- - - 
## Paragraphen 
Jeder Paragraph beginnt mit einer neuen Zeile. Drücke einfach "Enter" um einen neuen Paragraph zu beginnen.

- - - 
## Horizontale Linien
Du kannst horizontale Linien (<hr>) einfügen, indem du drei Sternchen (`*`), Minus (`-`) oder Unterstriche (`_`) unterteilt durch Leerzeichen eintippst. Zum Beispiel:
```
* * *
```
or
```
- - -
```

- - - 
## Ignorieren von Formatierungszeichen
Wenn du ein Formatierungszeichen wie Stern (`*`) oder Minus (`-`) verwenden möchtest, und **OneTwoWrite** soll es auch als ein solches Zeichen behandeln, setze davor einfach einen Backslash (`\`). Zum Beispiel: `\*nicht kursiv\*`, `\_nicht kursiv\_`. In Code-Blöcken musst du solche Demaskierungen nicht vornehmen.
